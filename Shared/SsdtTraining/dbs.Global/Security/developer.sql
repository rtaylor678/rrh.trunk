﻿CREATE ROLE [developer]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'DB';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'DBB';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'FRS';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'glc';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'JED';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'PRD';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'RMF';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'Saroy';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'developer', @membername = N'MGM';


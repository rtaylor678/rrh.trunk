﻿CREATE ROLE [datagrp]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'datagrp', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'datagrp', @membername = N'JED';


GO
EXECUTE sp_addrolemember @rolename = N'datagrp', @membername = N'RRH';


GO
EXECUTE sp_addrolemember @rolename = N'datagrp', @membername = N'SW';


﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'FRS';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'glc';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'Anyone';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'MWN';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'PWRAPPS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'EJB';


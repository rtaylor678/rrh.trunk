﻿
CREATE FUNCTION [dbo].[InflationFactor](@CurrCode char(4), @FromYear smallint, @ToYear smallint)
RETURNS real
AS
BEGIN
	RETURN dbo.InflationIndex(@CurrCode, @ToYear, 1)/dbo.InflationIndex(@CurrCode, @FromYear, 1)
END



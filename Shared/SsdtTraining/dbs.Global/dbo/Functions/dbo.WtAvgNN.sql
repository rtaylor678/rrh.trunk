﻿CREATE AGGREGATE [dbo].[WtAvgNN](@valueA FLOAT (53), @WtFactor FLOAT (53))
    RETURNS FLOAT (53)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.WtAvgNN];


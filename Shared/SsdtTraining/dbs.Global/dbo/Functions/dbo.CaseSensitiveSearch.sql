﻿
CREATE FUNCTION CaseSensitiveSearch(@strSearch varchar(255), @strFind char(1), @Start int = 1)
RETURNS int
BEGIN
	DECLARE @Result int, @asc int
	SELECT @asc = ASCII(@strFind)
	IF @Start <=0
		SET @Start = 0
	ELSE
		SELECT @Start = @Start-1
	WHILE @Start <= DATALENGTH(@strSearch)
	BEGIN
		SELECT @Start = @Start + 1
		IF ASCII(SUBSTRING(@strSearch, @Start, 1)) = @asc
			BREAK
	END
	IF @Start >= DATALENGTH(@strSearch)
		SET @Start = 0
	RETURN @Start
END



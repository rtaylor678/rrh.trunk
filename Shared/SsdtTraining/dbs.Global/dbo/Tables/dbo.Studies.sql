﻿CREATE TABLE [dbo].[Studies] (
    [Study]      CHAR (3)   NOT NULL,
    [StudyName]  CHAR (100) NOT NULL,
    [StudyGroup] CHAR (5)   NOT NULL,
    CONSTRAINT [PK__Studies__20C1E124] PRIMARY KEY CLUSTERED ([Study] ASC) WITH (FILLFACTOR = 90)
);


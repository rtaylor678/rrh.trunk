﻿CREATE TABLE [dbo].[UserPreferences] (
    [PreferenceID] CHAR (5)     NOT NULL,
    [Description]  VARCHAR (50) NULL,
    [CurrencyID]   INT          NULL,
    [LiqVolume]    CHAR (4)     NOT NULL,
    [GasVolume]    CHAR (4)     NOT NULL,
    [Weight]       CHAR (4)     NOT NULL,
    [ThermEnergy]  CHAR (4)     NOT NULL,
    [Temperature]  CHAR (4)     NOT NULL,
    [Pressure]     CHAR (4)     NOT NULL,
    [Gravity]      CHAR (4)     NOT NULL,
    [Viscosity]    CHAR (4)     NOT NULL,
    CONSTRAINT [PK_UserPreferences] PRIMARY KEY CLUSTERED ([PreferenceID] ASC)
);


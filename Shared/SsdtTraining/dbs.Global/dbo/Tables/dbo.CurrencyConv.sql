﻿CREATE TABLE [dbo].[CurrencyConv] (
    [CurrencyCode] CHAR (4)      NOT NULL,
    [Year]         INT           NOT NULL,
    [ConvRate]     REAL          NOT NULL,
    [SaveDate]     SMALLDATETIME CONSTRAINT [DF_Currency_SaveDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CurrencyConv] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC, [Year] ASC)
);


GO
/****** Object:  Trigger dbo.UpdateCurrencyDate    Script Date: 12/28/2001 7:32:01 AM ******/
/****** Object:  Trigger dbo.UpdateCurrencyDate    Script Date: 2/10/99 5:38:54 PM ******/
CREATE TRIGGER UpdateCurrencyDate ON dbo.CurrencyConv 
FOR UPDATE 
AS
UPDATE CurrencyConv
SET SaveDate = GetDate()
FROM deleted, CurrencyConv
WHERE deleted.CurrencyCode = CurrencyConv.CurrencyCode
AND deleted.Year = CurrencyConv.Year
AND deleted.ConvRate <> CurrencyConv.ConvRate

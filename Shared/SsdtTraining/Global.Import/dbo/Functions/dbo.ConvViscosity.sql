﻿
create function [dbo].[ConvViscosity](@valToConv float, @fromUnits varchar(255), @toUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @temp float, @Result float
	IF @toUnits = @FromUnits 
		SET @Result = @ValToConv
	ELSE IF @fromUnits = 'CS50C' 
		SET @Result = @ValToConv
	ELSE IF @fromUnits = 'CS122F' 
		SET @Result = @ValToConv
	ELSE IF @fromUnits = 'CS212F' 
		SET @Result = @ValToConv
	ELSE BEGIN
		IF @fromUnits = 'CS40C' 
		BEGIN -- Only convert to SSU100F
			-- First, calculate SSU104
			SELECT @temp = 4.6324*@ValToConv + (1+0.03264*@ValToconv)/
						((3930.2 + 262.7*@ValToConv + 23.97*SQUARE(@ValtoConv) +
						 1.646*Power(@ValToConv,3))/100000)
			SELECT @Result = @temp/(1+0.000061*(104-100))
		END
		ELSE IF @fromUnits = 'CS100C' -- Only convert to SSU210F
			SELECT @Result = (1+0.000061*(210-100))*(4.6324*@ValToConv + (1+0.03264*@ValToconv)/
						((3930.2 + 262.7*@ValToConv + 23.97*SQUARE(@ValtoConv) +
						 1.646*Power(@ValToconv,3))/100000))
		ELSE IF @FromUnits = 'SSU100F' 
		BEGIN  -- Only convert to CS40C
			-- Calculate SSU104
			SELECT @temp = @ValToConv * (1+0.000061*(104-100))
			SELECT @Result = 0.21587 * @temp - ((11069 + @temp)/(Power(@temp,3) + 37003))
		END
		ELSE IF @FromUnits = 'SSU210F' -- Only convert to CS100C
			SELECT @Result = 0.21443*@ValToConv - ((11219*@ValToConv)/(Power(@ValToConv, 3) + 37755))
		ELSE 
			SET @Result = NULL
	END
	RETURN @Result
END



﻿CREATE TABLE [dbo].[Currency_LU] (
    [CurrencyCode]    CHAR (4)     NOT NULL,
    [Country]         VARCHAR (30) NOT NULL,
    [Currency]        VARCHAR (30) NOT NULL,
    [KCurrency]       VARCHAR (32) NOT NULL,
    [LastYear]        SMALLINT     NULL,
    [NewCurrency]     CHAR (4)     NULL,
    [CentsName]       VARCHAR (30) NULL,
    [CentsMultiplier] REAL         NULL,
    CONSTRAINT [PK_Currency_LU] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC)
);


﻿INSERT INTO [peeps].[Names]
(
	[FirstName]
)
SELECT
	[n].[FirstName]
FROM (VALUES
	('Omar')
	) [n]([FirstName])
LEFT OUTER JOIN
	[peeps].[Names]	[x]
		ON	[x].[FirstName] = [n].[FirstName]
WHERE
	[x].[NamesId]	IS NULL;
Attribute VB_Name = "ExportXml"
Option Private Module
Option Explicit
Option Base 0
Option Compare Binary

Public Sub ExportXmlMap(ByVal wkb As Excel.Workbook, ByVal NameXmlMap As String, ByVal PathFileXml As String)
    
    wkb.XmlMaps(NameXmlMap).Export NameXmlMap, True

End Sub

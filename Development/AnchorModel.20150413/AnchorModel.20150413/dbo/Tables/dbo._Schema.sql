﻿CREATE TABLE [dbo].[_Schema] (
    [version]    INT           IDENTITY (1, 1) NOT NULL,
    [activation] DATETIME2 (7) NOT NULL,
    [schema]     XML           NOT NULL,
    PRIMARY KEY CLUSTERED ([version] ASC)
);


﻿CREATE TABLE [dbo].[CN_PHN_Contact_Phone_Posit] (
    [CN_PHN_ID]            INT          NOT NULL,
    [CN_PHN_CN_ID]         INT          NOT NULL,
    [CN_PHN_Contact_Phone] VARCHAR (20) NOT NULL,
    [CN_PHN_ChangedAt]     DATETIME     NOT NULL,
    CONSTRAINT [pkCN_PHN_Contact_Phone_Posit] PRIMARY KEY NONCLUSTERED ([CN_PHN_ID] ASC),
    CONSTRAINT [fkCN_PHN_Contact_Phone_Posit] FOREIGN KEY ([CN_PHN_CN_ID]) REFERENCES [dbo].[CN_Contact] ([CN_ID]),
    CONSTRAINT [uqCN_PHN_Contact_Phone_Posit] UNIQUE CLUSTERED ([CN_PHN_CN_ID] ASC, [CN_PHN_ChangedAt] DESC, [CN_PHN_Contact_Phone] ASC)
);


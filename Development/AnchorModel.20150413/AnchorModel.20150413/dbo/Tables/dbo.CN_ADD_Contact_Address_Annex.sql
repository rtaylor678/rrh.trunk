﻿CREATE TABLE [dbo].[CN_ADD_Contact_Address_Annex] (
    [CN_ADD_ID]          INT      NOT NULL,
    [CN_ADD_PositedAt]   DATETIME NOT NULL,
    [CN_ADD_Positor]     TINYINT  NOT NULL,
    [CN_ADD_Reliability] TINYINT  NOT NULL,
    [CN_ADD_Reliable]    AS       (isnull(CONVERT([tinyint],case when [CN_ADD_Reliability]<(1) then (0) else (1) end),(1))) PERSISTED NOT NULL,
    CONSTRAINT [pkCN_ADD_Contact_Address_Annex] PRIMARY KEY CLUSTERED ([CN_ADD_ID] ASC, [CN_ADD_Positor] ASC, [CN_ADD_PositedAt] DESC),
    CONSTRAINT [rcCN_ADD_Contact_Address_Annex] CHECK ([dbo].[rfCN_ADD_Contact_Address]([CN_ADD_ID],[CN_ADD_PositedAt],[CN_ADD_Positor],[CN_ADD_Reliable])=(0)),
    CONSTRAINT [fkCN_ADD_Contact_Address_Annex] FOREIGN KEY ([CN_ADD_ID]) REFERENCES [dbo].[CN_ADD_Contact_Address_Posit] ([CN_ADD_ID])
);


﻿CREATE TABLE [dbo].[CN_EML_Contact_EMail_Posit] (
    [CN_EML_ID]            INT           NOT NULL,
    [CN_EML_CN_ID]         INT           NOT NULL,
    [CN_EML_Contact_EMail] VARCHAR (254) NOT NULL,
    [CN_EML_ChangedAt]     DATETIME      NOT NULL,
    CONSTRAINT [pkCN_EML_Contact_EMail_Posit] PRIMARY KEY NONCLUSTERED ([CN_EML_ID] ASC),
    CONSTRAINT [fkCN_EML_Contact_EMail_Posit] FOREIGN KEY ([CN_EML_CN_ID]) REFERENCES [dbo].[CN_Contact] ([CN_ID]),
    CONSTRAINT [uqCN_EML_Contact_EMail_Posit] UNIQUE CLUSTERED ([CN_EML_CN_ID] ASC, [CN_EML_ChangedAt] DESC, [CN_EML_Contact_EMail] ASC)
);


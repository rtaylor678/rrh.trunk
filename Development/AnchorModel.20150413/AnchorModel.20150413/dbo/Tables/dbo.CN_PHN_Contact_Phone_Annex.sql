﻿CREATE TABLE [dbo].[CN_PHN_Contact_Phone_Annex] (
    [CN_PHN_ID]          INT      NOT NULL,
    [CN_PHN_PositedAt]   DATETIME NOT NULL,
    [CN_PHN_Positor]     TINYINT  NOT NULL,
    [CN_PHN_Reliability] TINYINT  NOT NULL,
    [CN_PHN_Reliable]    AS       (isnull(CONVERT([tinyint],case when [CN_PHN_Reliability]<(1) then (0) else (1) end),(1))) PERSISTED NOT NULL,
    CONSTRAINT [pkCN_PHN_Contact_Phone_Annex] PRIMARY KEY CLUSTERED ([CN_PHN_ID] ASC, [CN_PHN_Positor] ASC, [CN_PHN_PositedAt] DESC),
    CONSTRAINT [rcCN_PHN_Contact_Phone_Annex] CHECK ([dbo].[rfCN_PHN_Contact_Phone]([CN_PHN_ID],[CN_PHN_PositedAt],[CN_PHN_Positor],[CN_PHN_Reliable])=(0)),
    CONSTRAINT [fkCN_PHN_Contact_Phone_Annex] FOREIGN KEY ([CN_PHN_ID]) REFERENCES [dbo].[CN_PHN_Contact_Phone_Posit] ([CN_PHN_ID])
);


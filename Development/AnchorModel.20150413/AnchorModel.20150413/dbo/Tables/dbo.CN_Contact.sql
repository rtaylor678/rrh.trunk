﻿CREATE TABLE [dbo].[CN_Contact] (
    [CN_ID]    INT IDENTITY (1, 1) NOT NULL,
    [CN_Dummy] BIT NULL,
    CONSTRAINT [pkCN_Contact] PRIMARY KEY CLUSTERED ([CN_ID] ASC)
);


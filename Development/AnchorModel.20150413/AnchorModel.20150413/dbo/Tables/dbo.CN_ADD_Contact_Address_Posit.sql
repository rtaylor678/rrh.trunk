﻿CREATE TABLE [dbo].[CN_ADD_Contact_Address_Posit] (
    [CN_ADD_ID]              INT           NOT NULL,
    [CN_ADD_CN_ID]           INT           NOT NULL,
    [CN_ADD_Contact_Address] VARCHAR (128) NOT NULL,
    [CN_ADD_ChangedAt]       DATETIME      NOT NULL,
    CONSTRAINT [pkCN_ADD_Contact_Address_Posit] PRIMARY KEY NONCLUSTERED ([CN_ADD_ID] ASC),
    CONSTRAINT [fkCN_ADD_Contact_Address_Posit] FOREIGN KEY ([CN_ADD_CN_ID]) REFERENCES [dbo].[CN_Contact] ([CN_ID]),
    CONSTRAINT [uqCN_ADD_Contact_Address_Posit] UNIQUE CLUSTERED ([CN_ADD_CN_ID] ASC, [CN_ADD_ChangedAt] DESC, [CN_ADD_Contact_Address] ASC)
);


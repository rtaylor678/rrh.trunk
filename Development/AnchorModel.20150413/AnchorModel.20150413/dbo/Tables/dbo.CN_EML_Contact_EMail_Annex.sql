﻿CREATE TABLE [dbo].[CN_EML_Contact_EMail_Annex] (
    [CN_EML_ID]          INT      NOT NULL,
    [CN_EML_PositedAt]   DATETIME NOT NULL,
    [CN_EML_Positor]     TINYINT  NOT NULL,
    [CN_EML_Reliability] TINYINT  NOT NULL,
    [CN_EML_Reliable]    AS       (isnull(CONVERT([tinyint],case when [CN_EML_Reliability]<(1) then (0) else (1) end),(1))) PERSISTED NOT NULL,
    CONSTRAINT [pkCN_EML_Contact_EMail_Annex] PRIMARY KEY CLUSTERED ([CN_EML_ID] ASC, [CN_EML_Positor] ASC, [CN_EML_PositedAt] DESC),
    CONSTRAINT [rcCN_EML_Contact_EMail_Annex] CHECK ([dbo].[rfCN_EML_Contact_EMail]([CN_EML_ID],[CN_EML_PositedAt],[CN_EML_Positor],[CN_EML_Reliable])=(0)),
    CONSTRAINT [fkCN_EML_Contact_EMail_Annex] FOREIGN KEY ([CN_EML_ID]) REFERENCES [dbo].[CN_EML_Contact_EMail_Posit] ([CN_EML_ID])
);


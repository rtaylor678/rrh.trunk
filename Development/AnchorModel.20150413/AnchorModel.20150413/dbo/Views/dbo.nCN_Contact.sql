﻿-- Now perspective ----------------------------------------------------------------------------------------------------
-- nCN_Contact viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[nCN_Contact]
AS
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        sysdatetime(),
        DEFAULT,
        DEFAULT
    ) [CN];

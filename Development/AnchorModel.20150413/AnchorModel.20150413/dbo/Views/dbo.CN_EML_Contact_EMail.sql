﻿
    CREATE VIEW [dbo].[CN_EML_Contact_EMail]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_EML_ID,
        p.CN_EML_CN_ID,
        p.CN_EML_Contact_EMail,
        p.CN_EML_ChangedAt,
        a.CN_EML_PositedAt,
        a.CN_EML_Positor,
        a.CN_EML_Reliability,
        a.CN_EML_Reliable
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit] p
    JOIN
        [dbo].[CN_EML_Contact_EMail_Annex] a
    ON
        a.CN_EML_ID = p.CN_EML_ID;
    
GO
CREATE UNIQUE CLUSTERED INDEX [pkCN_EML_Contact_EMail]
    ON [dbo].[CN_EML_Contact_EMail]([CN_EML_Reliable] DESC, [CN_EML_CN_ID] ASC, [CN_EML_ChangedAt] DESC, [CN_EML_PositedAt] DESC, [CN_EML_Positor] ASC);


GO
CREATE TRIGGER [dbo].[it_CN_EML_Contact_EMail] ON [dbo].[CN_EML_Contact_EMail]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_EML_Contact_EMail TABLE (
        CN_EML_CN_ID int not null,
        CN_EML_ChangedAt datetime not null,
        CN_EML_Positor tinyint not null,
        CN_EML_PositedAt datetime not null,
        CN_EML_Reliability tinyint not null,
        CN_EML_Reliable tinyint not null,
        CN_EML_Contact_EMail VARCHAR(254) not null,
        CN_EML_Version bigint not null,
        CN_EML_StatementType char(1) not null,
        primary key(
            CN_EML_Version,
            CN_EML_Positor,
            CN_EML_CN_ID
        )
    );
    INSERT INTO @CN_EML_Contact_EMail
    SELECT
        i.CN_EML_CN_ID,
        i.CN_EML_ChangedAt,
        i.CN_EML_Positor,
        i.CN_EML_PositedAt,
        i.CN_EML_Reliability,
        case
            when i.CN_EML_Reliability < 1 then 0
            else 1
        end,
        i.CN_EML_Contact_EMail,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_EML_Positor,
                i.CN_EML_CN_ID
            ORDER BY
                i.CN_EML_ChangedAt ASC,
                i.CN_EML_PositedAt ASC,
                i.CN_EML_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_EML_Version),
        @currentVersion = 0
    FROM
        @CN_EML_Contact_EMail;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_EML_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_EML_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_EML_Positor, v.CN_EML_ChangedAt, v.CN_EML_PositedAt, v.CN_EML_Reliable) t
                        WHERE
                            t.CN_EML_CN_ID = v.CN_EML_CN_ID
                        AND
                            t.CN_EML_ChangedAt = v.CN_EML_ChangedAt
                        AND
                            t.CN_EML_Reliability = v.CN_EML_Reliability
                        AND
                            t.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_EML_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_EML_Contact_EMail
                        WHERE
                            v.CN_EML_Contact_EMail =
                                dbo.preCN_EML_Contact_EMail (
                                    v.CN_EML_CN_ID,
                                    v.CN_EML_Positor,
                                    v.CN_EML_ChangedAt,
                                    v.CN_EML_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_EML_Contact_EMail
                        WHERE
                            v.CN_EML_Contact_EMail =
                                dbo.folCN_EML_Contact_EMail (
                                    v.CN_EML_CN_ID,
                                    v.CN_EML_Positor,
                                    v.CN_EML_ChangedAt,
                                    v.CN_EML_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_EML_Contact_EMail v
        LEFT JOIN
            [dbo].[CN_EML_Contact_EMail_Posit] p
        ON
            p.CN_EML_CN_ID = v.CN_EML_CN_ID
        AND
            p.CN_EML_ChangedAt = v.CN_EML_ChangedAt
        AND
            p.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
        WHERE
            v.CN_EML_Version = @currentVersion;
        INSERT INTO [dbo].[CN_EML_Contact_EMail_Posit] (
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Contact_EMail
        )
        SELECT
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Contact_EMail
        FROM
            @CN_EML_Contact_EMail
        WHERE
            CN_EML_Version = @currentVersion
        AND
            CN_EML_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_EML_Contact_EMail_Annex] (
            CN_EML_ID,
            CN_EML_Positor,
            CN_EML_PositedAt,
            CN_EML_Reliability
        )
        SELECT
            p.CN_EML_ID,
            v.CN_EML_Positor,
            v.CN_EML_PositedAt,
            v.CN_EML_Reliability
        FROM
            @CN_EML_Contact_EMail v
        JOIN
            [dbo].[CN_EML_Contact_EMail_Posit] p
        ON
            p.CN_EML_CN_ID = v.CN_EML_CN_ID
        AND
            p.CN_EML_ChangedAt = v.CN_EML_ChangedAt
        AND
            p.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
        WHERE
            v.CN_EML_Version = @currentVersion
        AND
            CN_EML_StatementType in ('S','N','D','R');
    END
END

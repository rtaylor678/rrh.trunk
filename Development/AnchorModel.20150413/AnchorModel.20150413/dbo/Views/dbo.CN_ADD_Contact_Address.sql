﻿
    CREATE VIEW [dbo].[CN_ADD_Contact_Address]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_ADD_ID,
        p.CN_ADD_CN_ID,
        p.CN_ADD_Contact_Address,
        p.CN_ADD_ChangedAt,
        a.CN_ADD_PositedAt,
        a.CN_ADD_Positor,
        a.CN_ADD_Reliability,
        a.CN_ADD_Reliable
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit] p
    JOIN
        [dbo].[CN_ADD_Contact_Address_Annex] a
    ON
        a.CN_ADD_ID = p.CN_ADD_ID;
    
GO
CREATE UNIQUE CLUSTERED INDEX [pkCN_ADD_Contact_Address]
    ON [dbo].[CN_ADD_Contact_Address]([CN_ADD_Reliable] DESC, [CN_ADD_CN_ID] ASC, [CN_ADD_ChangedAt] DESC, [CN_ADD_PositedAt] DESC, [CN_ADD_Positor] ASC);


GO
CREATE TRIGGER [dbo].[it_CN_ADD_Contact_Address] ON [dbo].[CN_ADD_Contact_Address]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_ADD_Contact_Address TABLE (
        CN_ADD_CN_ID int not null,
        CN_ADD_ChangedAt datetime not null,
        CN_ADD_Positor tinyint not null,
        CN_ADD_PositedAt datetime not null,
        CN_ADD_Reliability tinyint not null,
        CN_ADD_Reliable tinyint not null,
        CN_ADD_Contact_Address VARCHAR(128) not null,
        CN_ADD_Version bigint not null,
        CN_ADD_StatementType char(1) not null,
        primary key(
            CN_ADD_Version,
            CN_ADD_Positor,
            CN_ADD_CN_ID
        )
    );
    INSERT INTO @CN_ADD_Contact_Address
    SELECT
        i.CN_ADD_CN_ID,
        i.CN_ADD_ChangedAt,
        i.CN_ADD_Positor,
        i.CN_ADD_PositedAt,
        i.CN_ADD_Reliability,
        case
            when i.CN_ADD_Reliability < 1 then 0
            else 1
        end,
        i.CN_ADD_Contact_Address,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_ADD_Positor,
                i.CN_ADD_CN_ID
            ORDER BY
                i.CN_ADD_ChangedAt ASC,
                i.CN_ADD_PositedAt ASC,
                i.CN_ADD_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_ADD_Version),
        @currentVersion = 0
    FROM
        @CN_ADD_Contact_Address;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_ADD_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_ADD_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_ADD_Positor, v.CN_ADD_ChangedAt, v.CN_ADD_PositedAt, v.CN_ADD_Reliable) t
                        WHERE
                            t.CN_ADD_CN_ID = v.CN_ADD_CN_ID
                        AND
                            t.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
                        AND
                            t.CN_ADD_Reliability = v.CN_ADD_Reliability
                        AND
                            t.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_ADD_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_ADD_Contact_Address
                        WHERE
                            v.CN_ADD_Contact_Address =
                                dbo.preCN_ADD_Contact_Address (
                                    v.CN_ADD_CN_ID,
                                    v.CN_ADD_Positor,
                                    v.CN_ADD_ChangedAt,
                                    v.CN_ADD_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_ADD_Contact_Address
                        WHERE
                            v.CN_ADD_Contact_Address =
                                dbo.folCN_ADD_Contact_Address (
                                    v.CN_ADD_CN_ID,
                                    v.CN_ADD_Positor,
                                    v.CN_ADD_ChangedAt,
                                    v.CN_ADD_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_ADD_Contact_Address v
        LEFT JOIN
            [dbo].[CN_ADD_Contact_Address_Posit] p
        ON
            p.CN_ADD_CN_ID = v.CN_ADD_CN_ID
        AND
            p.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
        AND
            p.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
        WHERE
            v.CN_ADD_Version = @currentVersion;
        INSERT INTO [dbo].[CN_ADD_Contact_Address_Posit] (
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Contact_Address
        )
        SELECT
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Contact_Address
        FROM
            @CN_ADD_Contact_Address
        WHERE
            CN_ADD_Version = @currentVersion
        AND
            CN_ADD_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_ADD_Contact_Address_Annex] (
            CN_ADD_ID,
            CN_ADD_Positor,
            CN_ADD_PositedAt,
            CN_ADD_Reliability
        )
        SELECT
            p.CN_ADD_ID,
            v.CN_ADD_Positor,
            v.CN_ADD_PositedAt,
            v.CN_ADD_Reliability
        FROM
            @CN_ADD_Contact_Address v
        JOIN
            [dbo].[CN_ADD_Contact_Address_Posit] p
        ON
            p.CN_ADD_CN_ID = v.CN_ADD_CN_ID
        AND
            p.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
        AND
            p.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
        WHERE
            v.CN_ADD_Version = @currentVersion
        AND
            CN_ADD_StatementType in ('S','N','D','R');
    END
END

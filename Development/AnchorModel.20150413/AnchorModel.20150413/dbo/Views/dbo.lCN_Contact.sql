﻿-- Latest perspective -------------------------------------------------------------------------------------------------
-- lCN_Contact viewed by the latest available information for all positors (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[lCN_Contact]
AS
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        DEFAULT,
        DEFAULT,
        DEFAULT
    ) [CN];

GO
-- ANCHOR TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers on the latest view make it behave like a table.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_lCN_Contact instead of INSERT trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[it_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    DECLARE @CN TABLE (
        Row bigint IDENTITY(1,1) not null primary key,
        CN_ID int not null
    );
    INSERT INTO [dbo].[CN_Contact] (
        CN_Dummy
    )
    OUTPUT
        inserted.CN_ID
    INTO
        @CN
    SELECT
        null
    FROM
        inserted
    WHERE
        inserted.CN_ID is null;
    DECLARE @inserted TABLE (
        CN_ID int not null,
        CN_ADD_CN_ID int null,
        CN_ADD_ChangedAt datetime null,
        CN_ADD_Positor tinyint null,
        CN_ADD_PositedAt datetime null,
        CN_ADD_Reliability tinyint null,
        CN_ADD_Contact_Address VARCHAR(128) null,
        CN_PHN_CN_ID int null,
        CN_PHN_ChangedAt datetime null,
        CN_PHN_Positor tinyint null,
        CN_PHN_PositedAt datetime null,
        CN_PHN_Reliability tinyint null,
        CN_PHN_Contact_Phone VARCHAR(20) null,
        CN_EML_CN_ID int null,
        CN_EML_ChangedAt datetime null,
        CN_EML_Positor tinyint null,
        CN_EML_PositedAt datetime null,
        CN_EML_Reliability tinyint null,
        CN_EML_Contact_EMail VARCHAR(254) null
    );
    INSERT INTO @inserted
    SELECT
        ISNULL(i.CN_ID, a.CN_ID),
        ISNULL(ISNULL(i.CN_ADD_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_ADD_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_ADD_Positor, i.Positor), 0),
        ISNULL(i.CN_ADD_PositedAt, @now),
        ISNULL(i.CN_ADD_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_ADD_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_ADD_Contact_Address,
        ISNULL(ISNULL(i.CN_PHN_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_PHN_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_PHN_Positor, i.Positor), 0),
        ISNULL(i.CN_PHN_PositedAt, @now),
        ISNULL(i.CN_PHN_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_PHN_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_PHN_Contact_Phone,
        ISNULL(ISNULL(i.CN_EML_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_EML_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_EML_Positor, i.Positor), 0),
        ISNULL(i.CN_EML_PositedAt, @now),
        ISNULL(i.CN_EML_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_EML_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_EML_Contact_EMail
    FROM (
        SELECT
            Positor,
            Reliable,
            CN_ID,
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Positor,
            CN_ADD_PositedAt,
            CN_ADD_Reliability,
            CN_ADD_Reliable,
            CN_ADD_Contact_Address,
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Positor,
            CN_PHN_PositedAt,
            CN_PHN_Reliability,
            CN_PHN_Reliable,
            CN_PHN_Contact_Phone,
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Positor,
            CN_EML_PositedAt,
            CN_EML_Reliability,
            CN_EML_Reliable,
            CN_EML_Contact_EMail,
            ROW_NUMBER() OVER (PARTITION BY CN_ID ORDER BY CN_ID) AS Row
        FROM
            inserted
    ) i
    LEFT JOIN
        @CN a
    ON
        a.Row = i.Row;
    INSERT INTO [dbo].[CN_ADD_Contact_Address] (
        CN_ADD_CN_ID,
        CN_ADD_Contact_Address,
        CN_ADD_ChangedAt,
        CN_ADD_PositedAt,
        CN_ADD_Positor,
        CN_ADD_Reliability
    )
    SELECT
        i.CN_ADD_CN_ID,
        i.CN_ADD_Contact_Address,
        i.CN_ADD_ChangedAt,
        i.CN_ADD_PositedAt,
        i.CN_ADD_Positor,
        i.CN_ADD_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_ADD_Contact_Address is not null;
    INSERT INTO [dbo].[CN_PHN_Contact_Phone] (
        CN_PHN_CN_ID,
        CN_PHN_Contact_Phone,
        CN_PHN_ChangedAt,
        CN_PHN_PositedAt,
        CN_PHN_Positor,
        CN_PHN_Reliability
    )
    SELECT
        i.CN_PHN_CN_ID,
        i.CN_PHN_Contact_Phone,
        i.CN_PHN_ChangedAt,
        i.CN_PHN_PositedAt,
        i.CN_PHN_Positor,
        i.CN_PHN_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_PHN_Contact_Phone is not null;
    INSERT INTO [dbo].[CN_EML_Contact_EMail] (
        CN_EML_CN_ID,
        CN_EML_Contact_EMail,
        CN_EML_ChangedAt,
        CN_EML_PositedAt,
        CN_EML_Positor,
        CN_EML_Reliability
    )
    SELECT
        i.CN_EML_CN_ID,
        i.CN_EML_Contact_EMail,
        i.CN_EML_ChangedAt,
        i.CN_EML_PositedAt,
        i.CN_EML_Positor,
        i.CN_EML_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_EML_Contact_EMail is not null;
END

GO
-- UPDATE trigger -----------------------------------------------------------------------------------------------------
-- ut_lCN_Contact instead of UPDATE trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[ut_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    IF(UPDATE(CN_ID))
        RAISERROR('The identity column CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_ID))
        RAISERROR('The identity column CN_ADD_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_CN_ID))
        RAISERROR('The foreign key column CN_ADD_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_Contact_Address))
    BEGIN
        INSERT INTO [dbo].[CN_ADD_Contact_Address] (
            CN_ADD_CN_ID,
            CN_ADD_Contact_Address,
            CN_ADD_ChangedAt,
            CN_ADD_PositedAt,
            CN_ADD_Positor,
            CN_ADD_Reliability
        )
        SELECT
            ISNULL(i.CN_ADD_CN_ID, i.CN_ID),
            i.CN_ADD_Contact_Address,
            cast(CASE
                WHEN i.CN_ADD_Contact_Address is null THEN i.CN_ADD_ChangedAt
                WHEN UPDATE(CN_ADD_ChangedAt) THEN i.CN_ADD_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_ADD_PositedAt) THEN i.CN_ADD_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_ADD_Positor, 0) END,
            CASE
                WHEN i.CN_ADD_Contact_Address is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_ADD_Reliable) THEN
                    CASE i.CN_ADD_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_ADD_Reliability, 1)
            END
        FROM
            inserted i
    END
    IF(UPDATE(CN_PHN_ID))
        RAISERROR('The identity column CN_PHN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_PHN_CN_ID))
        RAISERROR('The foreign key column CN_PHN_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_PHN_Contact_Phone))
    BEGIN
        INSERT INTO [dbo].[CN_PHN_Contact_Phone] (
            CN_PHN_CN_ID,
            CN_PHN_Contact_Phone,
            CN_PHN_ChangedAt,
            CN_PHN_PositedAt,
            CN_PHN_Positor,
            CN_PHN_Reliability
        )
        SELECT
            ISNULL(i.CN_PHN_CN_ID, i.CN_ID),
            i.CN_PHN_Contact_Phone,
            cast(CASE
                WHEN i.CN_PHN_Contact_Phone is null THEN i.CN_PHN_ChangedAt
                WHEN UPDATE(CN_PHN_ChangedAt) THEN i.CN_PHN_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_PHN_PositedAt) THEN i.CN_PHN_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_PHN_Positor, 0) END,
            CASE
                WHEN i.CN_PHN_Contact_Phone is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_PHN_Reliable) THEN
                    CASE i.CN_PHN_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_PHN_Reliability, 1)
            END
        FROM
            inserted i
    END
    IF(UPDATE(CN_EML_ID))
        RAISERROR('The identity column CN_EML_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_EML_CN_ID))
        RAISERROR('The foreign key column CN_EML_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_EML_Contact_EMail))
    BEGIN
        INSERT INTO [dbo].[CN_EML_Contact_EMail] (
            CN_EML_CN_ID,
            CN_EML_Contact_EMail,
            CN_EML_ChangedAt,
            CN_EML_PositedAt,
            CN_EML_Positor,
            CN_EML_Reliability
        )
        SELECT
            ISNULL(i.CN_EML_CN_ID, i.CN_ID),
            i.CN_EML_Contact_EMail,
            cast(CASE
                WHEN i.CN_EML_Contact_EMail is null THEN i.CN_EML_ChangedAt
                WHEN UPDATE(CN_EML_ChangedAt) THEN i.CN_EML_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_EML_PositedAt) THEN i.CN_EML_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_EML_Positor, 0) END,
            CASE
                WHEN i.CN_EML_Contact_EMail is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_EML_Reliable) THEN
                    CASE i.CN_EML_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_EML_Reliability, 1)
            END
        FROM
            inserted i
    END
END

GO
-- DELETE trigger -----------------------------------------------------------------------------------------------------
-- dt_lCN_Contact instead of DELETE trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[dt_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF DELETE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    INSERT INTO [dbo].[CN_ADD_Contact_Address_Annex] (
        CN_ADD_ID,
        CN_ADD_Positor,
        CN_ADD_PositedAt,
        CN_ADD_Reliability
    )
    SELECT
        p.CN_ADD_ID,
        p.CN_ADD_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_ADD_Contact_Address_Annex] p
    ON
        p.CN_ADD_ID = d.CN_ADD_ID;
    INSERT INTO [dbo].[CN_PHN_Contact_Phone_Annex] (
        CN_PHN_ID,
        CN_PHN_Positor,
        CN_PHN_PositedAt,
        CN_PHN_Reliability
    )
    SELECT
        p.CN_PHN_ID,
        p.CN_PHN_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_PHN_Contact_Phone_Annex] p
    ON
        p.CN_PHN_ID = d.CN_PHN_ID;
    INSERT INTO [dbo].[CN_EML_Contact_EMail_Annex] (
        CN_EML_ID,
        CN_EML_Positor,
        CN_EML_PositedAt,
        CN_EML_Reliability
    )
    SELECT
        p.CN_EML_ID,
        p.CN_EML_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_EML_Contact_EMail_Annex] p
    ON
        p.CN_EML_ID = d.CN_EML_ID;
END

﻿
    CREATE VIEW [dbo].[CN_PHN_Contact_Phone]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_PHN_ID,
        p.CN_PHN_CN_ID,
        p.CN_PHN_Contact_Phone,
        p.CN_PHN_ChangedAt,
        a.CN_PHN_PositedAt,
        a.CN_PHN_Positor,
        a.CN_PHN_Reliability,
        a.CN_PHN_Reliable
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit] p
    JOIN
        [dbo].[CN_PHN_Contact_Phone_Annex] a
    ON
        a.CN_PHN_ID = p.CN_PHN_ID;
    
GO
CREATE UNIQUE CLUSTERED INDEX [pkCN_PHN_Contact_Phone]
    ON [dbo].[CN_PHN_Contact_Phone]([CN_PHN_Reliable] DESC, [CN_PHN_CN_ID] ASC, [CN_PHN_ChangedAt] DESC, [CN_PHN_PositedAt] DESC, [CN_PHN_Positor] ASC);


GO
CREATE TRIGGER [dbo].[it_CN_PHN_Contact_Phone] ON [dbo].[CN_PHN_Contact_Phone]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_PHN_Contact_Phone TABLE (
        CN_PHN_CN_ID int not null,
        CN_PHN_ChangedAt datetime not null,
        CN_PHN_Positor tinyint not null,
        CN_PHN_PositedAt datetime not null,
        CN_PHN_Reliability tinyint not null,
        CN_PHN_Reliable tinyint not null,
        CN_PHN_Contact_Phone VARCHAR(20) not null,
        CN_PHN_Version bigint not null,
        CN_PHN_StatementType char(1) not null,
        primary key(
            CN_PHN_Version,
            CN_PHN_Positor,
            CN_PHN_CN_ID
        )
    );
    INSERT INTO @CN_PHN_Contact_Phone
    SELECT
        i.CN_PHN_CN_ID,
        i.CN_PHN_ChangedAt,
        i.CN_PHN_Positor,
        i.CN_PHN_PositedAt,
        i.CN_PHN_Reliability,
        case
            when i.CN_PHN_Reliability < 1 then 0
            else 1
        end,
        i.CN_PHN_Contact_Phone,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_PHN_Positor,
                i.CN_PHN_CN_ID
            ORDER BY
                i.CN_PHN_ChangedAt ASC,
                i.CN_PHN_PositedAt ASC,
                i.CN_PHN_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_PHN_Version),
        @currentVersion = 0
    FROM
        @CN_PHN_Contact_Phone;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_PHN_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_PHN_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_PHN_Positor, v.CN_PHN_ChangedAt, v.CN_PHN_PositedAt, v.CN_PHN_Reliable) t
                        WHERE
                            t.CN_PHN_CN_ID = v.CN_PHN_CN_ID
                        AND
                            t.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
                        AND
                            t.CN_PHN_Reliability = v.CN_PHN_Reliability
                        AND
                            t.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_PHN_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_PHN_Contact_Phone
                        WHERE
                            v.CN_PHN_Contact_Phone =
                                dbo.preCN_PHN_Contact_Phone (
                                    v.CN_PHN_CN_ID,
                                    v.CN_PHN_Positor,
                                    v.CN_PHN_ChangedAt,
                                    v.CN_PHN_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_PHN_Contact_Phone
                        WHERE
                            v.CN_PHN_Contact_Phone =
                                dbo.folCN_PHN_Contact_Phone (
                                    v.CN_PHN_CN_ID,
                                    v.CN_PHN_Positor,
                                    v.CN_PHN_ChangedAt,
                                    v.CN_PHN_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_PHN_Contact_Phone v
        LEFT JOIN
            [dbo].[CN_PHN_Contact_Phone_Posit] p
        ON
            p.CN_PHN_CN_ID = v.CN_PHN_CN_ID
        AND
            p.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
        AND
            p.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
        WHERE
            v.CN_PHN_Version = @currentVersion;
        INSERT INTO [dbo].[CN_PHN_Contact_Phone_Posit] (
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Contact_Phone
        )
        SELECT
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Contact_Phone
        FROM
            @CN_PHN_Contact_Phone
        WHERE
            CN_PHN_Version = @currentVersion
        AND
            CN_PHN_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_PHN_Contact_Phone_Annex] (
            CN_PHN_ID,
            CN_PHN_Positor,
            CN_PHN_PositedAt,
            CN_PHN_Reliability
        )
        SELECT
            p.CN_PHN_ID,
            v.CN_PHN_Positor,
            v.CN_PHN_PositedAt,
            v.CN_PHN_Reliability
        FROM
            @CN_PHN_Contact_Phone v
        JOIN
            [dbo].[CN_PHN_Contact_Phone_Posit] p
        ON
            p.CN_PHN_CN_ID = v.CN_PHN_CN_ID
        AND
            p.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
        AND
            p.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
        WHERE
            v.CN_PHN_Version = @currentVersion
        AND
            CN_PHN_StatementType in ('S','N','D','R');
    END
END

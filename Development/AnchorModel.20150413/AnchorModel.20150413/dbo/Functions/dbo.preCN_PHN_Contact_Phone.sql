﻿
    CREATE FUNCTION [dbo].[preCN_PHN_Contact_Phone] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(20)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_PHN_Contact_Phone
        FROM
            [dbo].[rCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_PHN_CN_ID = @id
        AND
            pre.CN_PHN_ChangedAt < @changingTimepoint
        AND
            pre.CN_PHN_Reliable = 1
        ORDER BY
            pre.CN_PHN_ChangedAt DESC,
            pre.CN_PHN_PositedAt DESC
    );
    END
    
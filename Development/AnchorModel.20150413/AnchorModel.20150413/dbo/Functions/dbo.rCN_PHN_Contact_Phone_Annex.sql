﻿
    CREATE FUNCTION [dbo].[rCN_PHN_Contact_Phone_Annex] (
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_PHN_ID,
        CN_PHN_PositedAt,
        CN_PHN_Positor,
        CN_PHN_Reliability,
        CN_PHN_Reliable
    FROM
        [dbo].[CN_PHN_Contact_Phone_Annex]
    WHERE
        CN_PHN_PositedAt <= @positingTimepoint;
    
﻿
    CREATE FUNCTION [dbo].[folCN_PHN_Contact_Phone] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(20)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_PHN_Contact_Phone
        FROM
            [dbo].[fCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_PHN_CN_ID = @id
        AND
            fol.CN_PHN_ChangedAt > @changingTimepoint
        AND
            fol.CN_PHN_Reliable = 1
        ORDER BY
            fol.CN_PHN_ChangedAt ASC,
            fol.CN_PHN_PositedAt DESC
    );
    END
    
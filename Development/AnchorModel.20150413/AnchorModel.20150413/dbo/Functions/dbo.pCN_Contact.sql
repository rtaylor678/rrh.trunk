﻿-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pCN_Contact viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[pCN_Contact] (
    @changingTimepoint datetime2(7)
)
RETURNS TABLE AS RETURN
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        @changingTimepoint,
        DEFAULT,
        DEFAULT
    ) [CN];

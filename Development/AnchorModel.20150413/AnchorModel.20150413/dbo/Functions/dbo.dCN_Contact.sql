﻿-- Difference perspective ---------------------------------------------------------------------------------------------
-- dCN_Contact showing all differences between the given timepoints and optionally for a subset of attributes
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[dCN_Contact] (
    @intervalStart datetime2(7),
    @intervalEnd datetime2(7),
    @selection varchar(max) = null
)
RETURNS TABLE AS RETURN
SELECT
    timepoints.inspectedTimepoint,
    [CN].*
FROM
    [dbo].[_Positor] p
JOIN (
    SELECT DISTINCT
        CN_ADD_Positor AS positor,
        CN_ADD_CN_ID AS CN_ID,
        CN_ADD_ChangedAt AS inspectedTimepoint,
        'ADD' AS mnemonic
    FROM
        [dbo].[CN_ADD_Contact_Address]
    WHERE
        (@selection is null OR @selection like '%ADD%')
    AND
        CN_ADD_ChangedAt BETWEEN @intervalStart AND @intervalEnd
    UNION
    SELECT DISTINCT
        CN_PHN_Positor AS positor,
        CN_PHN_CN_ID AS CN_ID,
        CN_PHN_ChangedAt AS inspectedTimepoint,
        'PHN' AS mnemonic
    FROM
        [dbo].[CN_PHN_Contact_Phone]
    WHERE
        (@selection is null OR @selection like '%PHN%')
    AND
        CN_PHN_ChangedAt BETWEEN @intervalStart AND @intervalEnd
    UNION
    SELECT DISTINCT
        CN_EML_Positor AS positor,
        CN_EML_CN_ID AS CN_ID,
        CN_EML_ChangedAt AS inspectedTimepoint,
        'EML' AS mnemonic
    FROM
        [dbo].[CN_EML_Contact_EMail]
    WHERE
        (@selection is null OR @selection like '%EML%')
    AND
        CN_EML_ChangedAt BETWEEN @intervalStart AND @intervalEnd
) timepoints
ON
    timepoints.positor = p.Positor
CROSS APPLY
    [dbo].[tCN_Contact] (
        timepoints.positor,
        timepoints.inspectedTimepoint,
        DEFAULT,
        DEFAULT
    ) [CN]
 WHERE
    [CN].CN_ID = timepoints.CN_ID;

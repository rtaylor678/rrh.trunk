﻿
    CREATE FUNCTION [dbo].[preCN_EML_Contact_EMail] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(254)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_EML_Contact_EMail
        FROM
            [dbo].[rCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_EML_CN_ID = @id
        AND
            pre.CN_EML_ChangedAt < @changingTimepoint
        AND
            pre.CN_EML_Reliable = 1
        ORDER BY
            pre.CN_EML_ChangedAt DESC,
            pre.CN_EML_PositedAt DESC
    );
    END
    
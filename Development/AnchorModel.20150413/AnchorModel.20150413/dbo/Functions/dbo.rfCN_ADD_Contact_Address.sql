﻿
    CREATE FUNCTION [dbo].[rfCN_ADD_Contact_Address] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(128);
    DECLARE @changed datetime;
    SELECT
        @id = CN_ADD_CN_ID,
        @value = CN_ADD_Contact_Address,
        @changed = CN_ADD_ChangedAt
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit]
    WHERE
        CN_ADD_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_ADD_Contact_Address
                    FROM
                        [dbo].[rCN_ADD_Contact_Address](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_ADD_CN_ID = @id
                    AND
                        pre.CN_ADD_ChangedAt < @changed
                    AND
                        pre.CN_ADD_Reliable = 1
                    ORDER BY
                        pre.CN_ADD_ChangedAt DESC,
                        pre.CN_ADD_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_ADD_Contact_Address
                    FROM
                        [dbo].[fCN_ADD_Contact_Address](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_ADD_CN_ID = @id
                    AND
                        fol.CN_ADD_ChangedAt > @changed
                    AND
                        fol.CN_ADD_Reliable = 1
                    ORDER BY
                        fol.CN_ADD_ChangedAt ASC,
                        fol.CN_ADD_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    
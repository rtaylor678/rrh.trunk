﻿
    CREATE FUNCTION [dbo].[rCN_ADD_Contact_Address_Annex] (
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_ADD_ID,
        CN_ADD_PositedAt,
        CN_ADD_Positor,
        CN_ADD_Reliability,
        CN_ADD_Reliable
    FROM
        [dbo].[CN_ADD_Contact_Address_Annex]
    WHERE
        CN_ADD_PositedAt <= @positingTimepoint;
    
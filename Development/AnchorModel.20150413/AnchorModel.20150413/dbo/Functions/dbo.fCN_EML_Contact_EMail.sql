﻿
    CREATE FUNCTION [dbo].[fCN_EML_Contact_EMail] (
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_EML_ID,
        a.CN_EML_PositedAt,
        a.CN_EML_Positor,
        a.CN_EML_Reliability,
        a.CN_EML_Reliable,
        p.CN_EML_CN_ID,
        p.CN_EML_Contact_EMail,
        p.CN_EML_ChangedAt
    FROM
        [dbo].[fCN_EML_Contact_EMail_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) a
    ON
        a.CN_EML_ID = p.CN_EML_ID
    AND
        a.CN_EML_Positor = @positor
    AND
        a.CN_EML_PositedAt = (
            SELECT TOP 1
                sub.CN_EML_PositedAt
            FROM
                [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_EML_ID = p.CN_EML_ID
            AND
                sub.CN_EML_Positor = @positor
            ORDER BY
                sub.CN_EML_PositedAt DESC
        )
    
﻿
    CREATE FUNCTION [dbo].[fCN_ADD_Contact_Address] (
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_ADD_ID,
        a.CN_ADD_PositedAt,
        a.CN_ADD_Positor,
        a.CN_ADD_Reliability,
        a.CN_ADD_Reliable,
        p.CN_ADD_CN_ID,
        p.CN_ADD_Contact_Address,
        p.CN_ADD_ChangedAt
    FROM
        [dbo].[fCN_ADD_Contact_Address_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) a
    ON
        a.CN_ADD_ID = p.CN_ADD_ID
    AND
        a.CN_ADD_Positor = @positor
    AND
        a.CN_ADD_PositedAt = (
            SELECT TOP 1
                sub.CN_ADD_PositedAt
            FROM
                [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_ADD_ID = p.CN_ADD_ID
            AND
                sub.CN_ADD_Positor = @positor
            ORDER BY
                sub.CN_ADD_PositedAt DESC
        )
    
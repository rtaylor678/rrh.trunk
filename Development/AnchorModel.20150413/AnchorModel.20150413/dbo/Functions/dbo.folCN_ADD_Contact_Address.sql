﻿
    CREATE FUNCTION [dbo].[folCN_ADD_Contact_Address] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(128)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_ADD_Contact_Address
        FROM
            [dbo].[fCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_ADD_CN_ID = @id
        AND
            fol.CN_ADD_ChangedAt > @changingTimepoint
        AND
            fol.CN_ADD_Reliable = 1
        ORDER BY
            fol.CN_ADD_ChangedAt ASC,
            fol.CN_ADD_PositedAt DESC
    );
    END
    
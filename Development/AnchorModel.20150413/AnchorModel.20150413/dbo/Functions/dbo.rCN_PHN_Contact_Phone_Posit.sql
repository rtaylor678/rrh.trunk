﻿
    CREATE FUNCTION [dbo].[rCN_PHN_Contact_Phone_Posit] (
        @changingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_PHN_ID,
        CN_PHN_CN_ID,
        CN_PHN_Contact_Phone,
        CN_PHN_ChangedAt
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit]
    WHERE
        CN_PHN_ChangedAt <= @changingTimepoint;
    
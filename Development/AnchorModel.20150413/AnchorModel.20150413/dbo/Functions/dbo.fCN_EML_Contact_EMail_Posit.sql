﻿
    CREATE FUNCTION [dbo].[fCN_EML_Contact_EMail_Posit] (
        @changingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_EML_ID,
        CN_EML_CN_ID,
        CN_EML_Contact_EMail,
        CN_EML_ChangedAt
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit]
    WHERE
        CN_EML_ChangedAt > @changingTimepoint;
    
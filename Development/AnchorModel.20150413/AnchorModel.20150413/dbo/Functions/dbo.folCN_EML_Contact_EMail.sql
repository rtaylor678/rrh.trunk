﻿
    CREATE FUNCTION [dbo].[folCN_EML_Contact_EMail] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(254)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_EML_Contact_EMail
        FROM
            [dbo].[fCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_EML_CN_ID = @id
        AND
            fol.CN_EML_ChangedAt > @changingTimepoint
        AND
            fol.CN_EML_Reliable = 1
        ORDER BY
            fol.CN_EML_ChangedAt ASC,
            fol.CN_EML_PositedAt DESC
    );
    END
    
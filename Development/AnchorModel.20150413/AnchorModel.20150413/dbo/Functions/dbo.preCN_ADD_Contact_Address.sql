﻿
    CREATE FUNCTION [dbo].[preCN_ADD_Contact_Address] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS VARCHAR(128)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_ADD_Contact_Address
        FROM
            [dbo].[rCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_ADD_CN_ID = @id
        AND
            pre.CN_ADD_ChangedAt < @changingTimepoint
        AND
            pre.CN_ADD_Reliable = 1
        ORDER BY
            pre.CN_ADD_ChangedAt DESC,
            pre.CN_ADD_PositedAt DESC
    );
    END
    
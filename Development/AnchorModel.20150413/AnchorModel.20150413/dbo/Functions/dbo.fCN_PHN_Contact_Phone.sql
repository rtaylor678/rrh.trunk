﻿
    CREATE FUNCTION [dbo].[fCN_PHN_Contact_Phone] (
        @positor tinyint = 0,
        @changingTimepoint datetime = '9999-12-31',
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_PHN_ID,
        a.CN_PHN_PositedAt,
        a.CN_PHN_Positor,
        a.CN_PHN_Reliability,
        a.CN_PHN_Reliable,
        p.CN_PHN_CN_ID,
        p.CN_PHN_Contact_Phone,
        p.CN_PHN_ChangedAt
    FROM
        [dbo].[fCN_PHN_Contact_Phone_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) a
    ON
        a.CN_PHN_ID = p.CN_PHN_ID
    AND
        a.CN_PHN_Positor = @positor
    AND
        a.CN_PHN_PositedAt = (
            SELECT TOP 1
                sub.CN_PHN_PositedAt
            FROM
                [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_PHN_ID = p.CN_PHN_ID
            AND
                sub.CN_PHN_Positor = @positor
            ORDER BY
                sub.CN_PHN_PositedAt DESC
        )
    
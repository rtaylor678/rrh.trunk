﻿
    CREATE FUNCTION [dbo].[rCN_EML_Contact_EMail_Annex] (
        @positingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_EML_ID,
        CN_EML_PositedAt,
        CN_EML_Positor,
        CN_EML_Reliability,
        CN_EML_Reliable
    FROM
        [dbo].[CN_EML_Contact_EMail_Annex]
    WHERE
        CN_EML_PositedAt <= @positingTimepoint;
    
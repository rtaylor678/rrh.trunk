﻿
    CREATE FUNCTION [dbo].[rfCN_PHN_Contact_Phone] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(20);
    DECLARE @changed datetime;
    SELECT
        @id = CN_PHN_CN_ID,
        @value = CN_PHN_Contact_Phone,
        @changed = CN_PHN_ChangedAt
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit]
    WHERE
        CN_PHN_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_PHN_Contact_Phone
                    FROM
                        [dbo].[rCN_PHN_Contact_Phone](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_PHN_CN_ID = @id
                    AND
                        pre.CN_PHN_ChangedAt < @changed
                    AND
                        pre.CN_PHN_Reliable = 1
                    ORDER BY
                        pre.CN_PHN_ChangedAt DESC,
                        pre.CN_PHN_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_PHN_Contact_Phone
                    FROM
                        [dbo].[fCN_PHN_Contact_Phone](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_PHN_CN_ID = @id
                    AND
                        fol.CN_PHN_ChangedAt > @changed
                    AND
                        fol.CN_PHN_Reliable = 1
                    ORDER BY
                        fol.CN_PHN_ChangedAt ASC,
                        fol.CN_PHN_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    
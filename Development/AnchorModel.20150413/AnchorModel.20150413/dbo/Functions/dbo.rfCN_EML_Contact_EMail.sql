﻿
    CREATE FUNCTION [dbo].[rfCN_EML_Contact_EMail] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(254);
    DECLARE @changed datetime;
    SELECT
        @id = CN_EML_CN_ID,
        @value = CN_EML_Contact_EMail,
        @changed = CN_EML_ChangedAt
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit]
    WHERE
        CN_EML_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_EML_Contact_EMail
                    FROM
                        [dbo].[rCN_EML_Contact_EMail](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_EML_CN_ID = @id
                    AND
                        pre.CN_EML_ChangedAt < @changed
                    AND
                        pre.CN_EML_Reliable = 1
                    ORDER BY
                        pre.CN_EML_ChangedAt DESC,
                        pre.CN_EML_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_EML_Contact_EMail
                    FROM
                        [dbo].[fCN_EML_Contact_EMail](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_EML_CN_ID = @id
                    AND
                        fol.CN_EML_ChangedAt > @changed
                    AND
                        fol.CN_EML_Reliable = 1
                    ORDER BY
                        fol.CN_EML_ChangedAt ASC,
                        fol.CN_EML_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    
﻿
    CREATE FUNCTION [dbo].[fCN_ADD_Contact_Address_Posit] (
        @changingTimepoint datetime = '9999-12-31'
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_ADD_ID,
        CN_ADD_CN_ID,
        CN_ADD_Contact_Address,
        CN_ADD_ChangedAt
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit]
    WHERE
        CN_ADD_ChangedAt > @changingTimepoint;
    
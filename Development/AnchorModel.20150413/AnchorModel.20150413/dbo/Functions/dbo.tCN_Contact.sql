﻿-- Time traveling perspective -----------------------------------------------------------------------------------------
-- tCN_Contact viewed as given by the input parameters
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[tCN_Contact] (
    @positor tinyint = 0,
    @changingTimepoint datetime2(7) = '9999-12-31',
    @positingTimepoint datetime = '9999-12-31',
    @reliable tinyint = 1
)
RETURNS TABLE WITH SCHEMABINDING AS RETURN
SELECT
    [CN].CN_ID,
    [ADD].CN_ADD_CN_ID,
    [ADD].CN_ADD_ID,
    [ADD].CN_ADD_ChangedAt,
    [ADD].CN_ADD_PositedAt,
    [ADD].CN_ADD_Positor,
    [ADD].CN_ADD_Reliability,
    [ADD].CN_ADD_Reliable,
    [ADD].CN_ADD_Contact_Address,
    [PHN].CN_PHN_CN_ID,
    [PHN].CN_PHN_ID,
    [PHN].CN_PHN_ChangedAt,
    [PHN].CN_PHN_PositedAt,
    [PHN].CN_PHN_Positor,
    [PHN].CN_PHN_Reliability,
    [PHN].CN_PHN_Reliable,
    [PHN].CN_PHN_Contact_Phone,
    [EML].CN_EML_CN_ID,
    [EML].CN_EML_ID,
    [EML].CN_EML_ChangedAt,
    [EML].CN_EML_PositedAt,
    [EML].CN_EML_Positor,
    [EML].CN_EML_Reliability,
    [EML].CN_EML_Reliable,
    [EML].CN_EML_Contact_EMail
FROM
    [dbo].[CN_Contact] [CN]
LEFT JOIN
    [dbo].[rCN_ADD_Contact_Address](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [ADD]
ON
    [ADD].CN_ADD_ID = (
        SELECT TOP 1
            sub.CN_ADD_ID
        FROM
            [dbo].[rCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_ADD_CN_ID = [CN].CN_ID
        AND
            sub.CN_ADD_Reliable = @reliable
        ORDER BY
            sub.CN_ADD_ChangedAt DESC,
            sub.CN_ADD_PositedAt DESC
    )
LEFT JOIN
    [dbo].[rCN_PHN_Contact_Phone](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [PHN]
ON
    [PHN].CN_PHN_ID = (
        SELECT TOP 1
            sub.CN_PHN_ID
        FROM
            [dbo].[rCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_PHN_CN_ID = [CN].CN_ID
        AND
            sub.CN_PHN_Reliable = @reliable
        ORDER BY
            sub.CN_PHN_ChangedAt DESC,
            sub.CN_PHN_PositedAt DESC
    )
LEFT JOIN
    [dbo].[rCN_EML_Contact_EMail](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [EML]
ON
    [EML].CN_EML_ID = (
        SELECT TOP 1
            sub.CN_EML_ID
        FROM
            [dbo].[rCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_EML_CN_ID = [CN].CN_ID
        AND
            sub.CN_EML_Reliable = @reliable
        ORDER BY
            sub.CN_EML_ChangedAt DESC,
            sub.CN_EML_PositedAt DESC
    );

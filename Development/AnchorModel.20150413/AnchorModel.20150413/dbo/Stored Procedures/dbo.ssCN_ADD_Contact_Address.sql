﻿CREATE PROCEDURE [dbo].[ssCN_ADD_Contact_Address] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_ADD_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_ADD_Contact_Address]
        WHERE
            CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_ADD_Positor
    FROM
        [dbo].[CN_ADD_Contact_Address]
    WHERE
        CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
    AND
        CN_ADD_Contact_Address is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_ADD_Positor,
                x.CN_ADD_CN_ID,
                x.CN_ADD_PositedAt,
                b.CN_ADD_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_ADD_CN_ID,
                    CN_ADD_PositedAt
                FROM
                    [dbo].[CN_ADD_Contact_Address]
                WHERE
                    CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_ADD_Positor,
                    CN_ADD_CN_ID,
                    CN_ADD_PositedAt,
                    CN_ADD_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_ADD_Contact_Address]
                PIVOT (
                    MIN(CN_ADD_Contact_Address) FOR CN_ADD_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
                AND
                    CN_ADD_Positor = ' + @positor + '
            ) b
            ON
                b.CN_ADD_CN_ID = x.CN_ADD_CN_ID
            AND
                b.CN_ADD_PositedAt = x.CN_ADD_PositedAt
            ORDER BY
                CN_ADD_CN_ID,
                CN_ADD_PositedAt,
                CN_ADD_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END

﻿CREATE PROCEDURE [dbo].[ssCN_PHN_Contact_Phone] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_PHN_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_PHN_Contact_Phone]
        WHERE
            CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_PHN_Positor
    FROM
        [dbo].[CN_PHN_Contact_Phone]
    WHERE
        CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
    AND
        CN_PHN_Contact_Phone is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_PHN_Positor,
                x.CN_PHN_CN_ID,
                x.CN_PHN_PositedAt,
                b.CN_PHN_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_PHN_CN_ID,
                    CN_PHN_PositedAt
                FROM
                    [dbo].[CN_PHN_Contact_Phone]
                WHERE
                    CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_PHN_Positor,
                    CN_PHN_CN_ID,
                    CN_PHN_PositedAt,
                    CN_PHN_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_PHN_Contact_Phone]
                PIVOT (
                    MIN(CN_PHN_Contact_Phone) FOR CN_PHN_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
                AND
                    CN_PHN_Positor = ' + @positor + '
            ) b
            ON
                b.CN_PHN_CN_ID = x.CN_PHN_CN_ID
            AND
                b.CN_PHN_PositedAt = x.CN_PHN_PositedAt
            ORDER BY
                CN_PHN_CN_ID,
                CN_PHN_PositedAt,
                CN_PHN_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END

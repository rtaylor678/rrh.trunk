﻿CREATE PROCEDURE [dbo].[ssCN_EML_Contact_EMail] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_EML_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_EML_Contact_EMail]
        WHERE
            CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_EML_Positor
    FROM
        [dbo].[CN_EML_Contact_EMail]
    WHERE
        CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
    AND
        CN_EML_Contact_EMail is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_EML_Positor,
                x.CN_EML_CN_ID,
                x.CN_EML_PositedAt,
                b.CN_EML_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_EML_CN_ID,
                    CN_EML_PositedAt
                FROM
                    [dbo].[CN_EML_Contact_EMail]
                WHERE
                    CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_EML_Positor,
                    CN_EML_CN_ID,
                    CN_EML_PositedAt,
                    CN_EML_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_EML_Contact_EMail]
                PIVOT (
                    MIN(CN_EML_Contact_EMail) FOR CN_EML_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
                AND
                    CN_EML_Positor = ' + @positor + '
            ) b
            ON
                b.CN_EML_CN_ID = x.CN_EML_CN_ID
            AND
                b.CN_EML_PositedAt = x.CN_EML_PositedAt
            ORDER BY
                CN_EML_CN_ID,
                CN_EML_PositedAt,
                CN_EML_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END

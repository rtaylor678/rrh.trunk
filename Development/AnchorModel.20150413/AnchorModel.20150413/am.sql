﻿-- POSITOR METADATA ---------------------------------------------------------------------------------------------------
--
-- Sets up a table containing the list of available positors. Since at least one positor
-- must be available the table is set up with a default positor with identity 0.
--
-- Positor table ------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Positor', 'U') IS NULL
BEGIN
    CREATE TABLE [dbo].[_Positor] (
        Positor tinyint not null,
        constraint pk_Positor primary key (
            Positor asc
        )
    );
    INSERT INTO [dbo].[_Positor] (
        Positor
    )
    VALUES (
        0 -- the default positor
    );
END
GO
-- KNOTS --------------------------------------------------------------------------------------------------------------
--
-- Knots are used to store finite sets of values, normally used to describe states
-- of entities (through knotted attributes) or relationships (through knotted ties).
-- Knots have their own surrogate identities and are therefore immutable.
-- Values can be added to the set over time though.
-- Knots should have values that are mutually exclusive and exhaustive.
--
-- ANCHORS AND ATTRIBUTES ---------------------------------------------------------------------------------------------
--
-- Anchors are used to store the identities of entities.
-- Anchors are immutable.
-- Attributes are used to store values for properties of entities.
-- Attributes are mutable, their values may change over one or more types of time.
-- Attributes have four flavors: static, historized, knotted static, and knotted historized.
-- Anchors may have zero or more adjoined attributes.
--
-- Anchor table -------------------------------------------------------------------------------------------------------
-- CN_Contact table (with 3 attributes)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_Contact', 'U') IS NULL
CREATE TABLE [dbo].[CN_Contact] (
    CN_ID int IDENTITY(1,1) not null,
    CN_Dummy bit null,
    constraint pkCN_Contact primary key (
        CN_ID asc
    )
);
GO
-- Historized attribute posit table -----------------------------------------------------------------------------------
-- CN_ADD_Contact_Address_Posit table (on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_ADD_Contact_Address_Posit', 'U') IS NULL
CREATE TABLE [dbo].[CN_ADD_Contact_Address_Posit] (
    CN_ADD_ID int not null,
    CN_ADD_CN_ID int not null,
    CN_ADD_Contact_Address VARCHAR(128) not null,
    CN_ADD_ChangedAt datetime not null,
    constraint fkCN_ADD_Contact_Address_Posit foreign key (
        CN_ADD_CN_ID
    ) references [dbo].[CN_Contact](CN_ID),
    constraint pkCN_ADD_Contact_Address_Posit primary key nonclustered (
        CN_ADD_ID asc
    ),
    constraint uqCN_ADD_Contact_Address_Posit unique clustered (
        CN_ADD_CN_ID asc,
        CN_ADD_ChangedAt desc,
        CN_ADD_Contact_Address asc
    )
);
GO
-- Attribute annex table ----------------------------------------------------------------------------------------------
-- CN_ADD_Contact_Address_Annex table (of CN_ADD_Contact_Address_Posit on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_ADD_Contact_Address_Annex', 'U') IS NULL
CREATE TABLE [dbo].[CN_ADD_Contact_Address_Annex] (
    CN_ADD_ID int not null,
    CN_ADD_PositedAt datetime not null,
    CN_ADD_Positor tinyint not null,
    CN_ADD_Reliability tinyint not null,
    CN_ADD_Reliable as isnull(cast(
        case
            when CN_ADD_Reliability < 1 then 0
            else 1
        end
    as tinyint), 1) persisted,
    constraint fkCN_ADD_Contact_Address_Annex foreign key (
        CN_ADD_ID
    ) references [dbo].[CN_ADD_Contact_Address_Posit](CN_ADD_ID),
    constraint pkCN_ADD_Contact_Address_Annex primary key clustered (
        CN_ADD_ID asc,
        CN_ADD_Positor asc,
        CN_ADD_PositedAt desc
    )
);
GO
-- Historized attribute posit table -----------------------------------------------------------------------------------
-- CN_PHN_Contact_Phone_Posit table (on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_PHN_Contact_Phone_Posit', 'U') IS NULL
CREATE TABLE [dbo].[CN_PHN_Contact_Phone_Posit] (
    CN_PHN_ID int not null,
    CN_PHN_CN_ID int not null,
    CN_PHN_Contact_Phone VARCHAR(20) not null,
    CN_PHN_ChangedAt datetime not null,
    constraint fkCN_PHN_Contact_Phone_Posit foreign key (
        CN_PHN_CN_ID
    ) references [dbo].[CN_Contact](CN_ID),
    constraint pkCN_PHN_Contact_Phone_Posit primary key nonclustered (
        CN_PHN_ID asc
    ),
    constraint uqCN_PHN_Contact_Phone_Posit unique clustered (
        CN_PHN_CN_ID asc,
        CN_PHN_ChangedAt desc,
        CN_PHN_Contact_Phone asc
    )
);
GO
-- Attribute annex table ----------------------------------------------------------------------------------------------
-- CN_PHN_Contact_Phone_Annex table (of CN_PHN_Contact_Phone_Posit on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_PHN_Contact_Phone_Annex', 'U') IS NULL
CREATE TABLE [dbo].[CN_PHN_Contact_Phone_Annex] (
    CN_PHN_ID int not null,
    CN_PHN_PositedAt datetime not null,
    CN_PHN_Positor tinyint not null,
    CN_PHN_Reliability tinyint not null,
    CN_PHN_Reliable as isnull(cast(
        case
            when CN_PHN_Reliability < 1 then 0
            else 1
        end
    as tinyint), 1) persisted,
    constraint fkCN_PHN_Contact_Phone_Annex foreign key (
        CN_PHN_ID
    ) references [dbo].[CN_PHN_Contact_Phone_Posit](CN_PHN_ID),
    constraint pkCN_PHN_Contact_Phone_Annex primary key clustered (
        CN_PHN_ID asc,
        CN_PHN_Positor asc,
        CN_PHN_PositedAt desc
    )
);
GO
-- Historized attribute posit table -----------------------------------------------------------------------------------
-- CN_EML_Contact_EMail_Posit table (on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_EML_Contact_EMail_Posit', 'U') IS NULL
CREATE TABLE [dbo].[CN_EML_Contact_EMail_Posit] (
    CN_EML_ID int not null,
    CN_EML_CN_ID int not null,
    CN_EML_Contact_EMail VARCHAR(254) not null,
    CN_EML_ChangedAt datetime not null,
    constraint fkCN_EML_Contact_EMail_Posit foreign key (
        CN_EML_CN_ID
    ) references [dbo].[CN_Contact](CN_ID),
    constraint pkCN_EML_Contact_EMail_Posit primary key nonclustered (
        CN_EML_ID asc
    ),
    constraint uqCN_EML_Contact_EMail_Posit unique clustered (
        CN_EML_CN_ID asc,
        CN_EML_ChangedAt desc,
        CN_EML_Contact_EMail asc
    )
);
GO
-- Attribute annex table ----------------------------------------------------------------------------------------------
-- CN_EML_Contact_EMail_Annex table (of CN_EML_Contact_EMail_Posit on CN_Contact)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_EML_Contact_EMail_Annex', 'U') IS NULL
CREATE TABLE [dbo].[CN_EML_Contact_EMail_Annex] (
    CN_EML_ID int not null,
    CN_EML_PositedAt datetime not null,
    CN_EML_Positor tinyint not null,
    CN_EML_Reliability tinyint not null,
    CN_EML_Reliable as isnull(cast(
        case
            when CN_EML_Reliability < 1 then 0
            else 1
        end
    as tinyint), 1) persisted,
    constraint fkCN_EML_Contact_EMail_Annex foreign key (
        CN_EML_ID
    ) references [dbo].[CN_EML_Contact_EMail_Posit](CN_EML_ID),
    constraint pkCN_EML_Contact_EMail_Annex primary key clustered (
        CN_EML_ID asc,
        CN_EML_Positor asc,
        CN_EML_PositedAt desc
    )
);
GO
-- ATTRIBUTE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------
--
-- The assembled view of an attribute combines the posit and annex table of the attribute.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- Attribute assembled view -------------------------------------------------------------------------------------------
-- CN_ADD_Contact_Address assembled view of the posit and annex tables,
-- pkCN_ADD_Contact_Address optional temporal consistency constraint
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_ADD_Contact_Address', 'V') IS NULL
BEGIN
    EXEC('
    CREATE VIEW [dbo].[CN_ADD_Contact_Address]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_ADD_ID,
        p.CN_ADD_CN_ID,
        p.CN_ADD_Contact_Address,
        p.CN_ADD_ChangedAt,
        a.CN_ADD_PositedAt,
        a.CN_ADD_Positor,
        a.CN_ADD_Reliability,
        a.CN_ADD_Reliable
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit] p
    JOIN
        [dbo].[CN_ADD_Contact_Address_Annex] a
    ON
        a.CN_ADD_ID = p.CN_ADD_ID;
    ');
    -- Constraint ensuring that recorded and erased posits are temporally consistent
    EXEC('
    CREATE UNIQUE CLUSTERED INDEX [pkCN_ADD_Contact_Address]
    ON [dbo].[CN_ADD_Contact_Address] (
        CN_ADD_Reliable desc,
        CN_ADD_CN_ID asc,
        CN_ADD_ChangedAt desc,
        CN_ADD_PositedAt desc,
        CN_ADD_Positor asc
    );
    ');
END
GO
-- Attribute assembled view -------------------------------------------------------------------------------------------
-- CN_PHN_Contact_Phone assembled view of the posit and annex tables,
-- pkCN_PHN_Contact_Phone optional temporal consistency constraint
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_PHN_Contact_Phone', 'V') IS NULL
BEGIN
    EXEC('
    CREATE VIEW [dbo].[CN_PHN_Contact_Phone]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_PHN_ID,
        p.CN_PHN_CN_ID,
        p.CN_PHN_Contact_Phone,
        p.CN_PHN_ChangedAt,
        a.CN_PHN_PositedAt,
        a.CN_PHN_Positor,
        a.CN_PHN_Reliability,
        a.CN_PHN_Reliable
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit] p
    JOIN
        [dbo].[CN_PHN_Contact_Phone_Annex] a
    ON
        a.CN_PHN_ID = p.CN_PHN_ID;
    ');
    -- Constraint ensuring that recorded and erased posits are temporally consistent
    EXEC('
    CREATE UNIQUE CLUSTERED INDEX [pkCN_PHN_Contact_Phone]
    ON [dbo].[CN_PHN_Contact_Phone] (
        CN_PHN_Reliable desc,
        CN_PHN_CN_ID asc,
        CN_PHN_ChangedAt desc,
        CN_PHN_PositedAt desc,
        CN_PHN_Positor asc
    );
    ');
END
GO
-- Attribute assembled view -------------------------------------------------------------------------------------------
-- CN_EML_Contact_EMail assembled view of the posit and annex tables,
-- pkCN_EML_Contact_EMail optional temporal consistency constraint
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.CN_EML_Contact_EMail', 'V') IS NULL
BEGIN
    EXEC('
    CREATE VIEW [dbo].[CN_EML_Contact_EMail]
    WITH SCHEMABINDING AS
    SELECT
        p.CN_EML_ID,
        p.CN_EML_CN_ID,
        p.CN_EML_Contact_EMail,
        p.CN_EML_ChangedAt,
        a.CN_EML_PositedAt,
        a.CN_EML_Positor,
        a.CN_EML_Reliability,
        a.CN_EML_Reliable
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit] p
    JOIN
        [dbo].[CN_EML_Contact_EMail_Annex] a
    ON
        a.CN_EML_ID = p.CN_EML_ID;
    ');
    -- Constraint ensuring that recorded and erased posits are temporally consistent
    EXEC('
    CREATE UNIQUE CLUSTERED INDEX [pkCN_EML_Contact_EMail]
    ON [dbo].[CN_EML_Contact_EMail] (
        CN_EML_Reliable desc,
        CN_EML_CN_ID asc,
        CN_EML_ChangedAt desc,
        CN_EML_PositedAt desc,
        CN_EML_Positor asc
    );
    ');
END
GO
-- ATTRIBUTE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------
--
-- These table valued functions rewind an attribute posit table to the given
-- point in changing time, or an attribute annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that the 
-- union of the two will produce all rows in a posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- Attribute posit rewinder -------------------------------------------------------------------------------------------
-- rCN_ADD_Contact_Address_Posit rewinding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_ADD_Contact_Address_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_ADD_Contact_Address_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_ADD_ID,
        CN_ADD_CN_ID,
        CN_ADD_Contact_Address,
        CN_ADD_ChangedAt
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit]
    WHERE
        CN_ADD_ChangedAt <= @changingTimepoint;
    ');
END
GO
-- Attribute posit forwarder ------------------------------------------------------------------------------------------
-- fCN_ADD_Contact_Address_Posit forwarding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_ADD_Contact_Address_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_ADD_Contact_Address_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_ADD_ID,
        CN_ADD_CN_ID,
        CN_ADD_Contact_Address,
        CN_ADD_ChangedAt
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit]
    WHERE
        CN_ADD_ChangedAt > @changingTimepoint;
    ');
END
GO
-- Attribute annex rewinder -------------------------------------------------------------------------------------------
-- rCN_ADD_Contact_Address_Annex rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_ADD_Contact_Address_Annex','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_ADD_Contact_Address_Annex] (
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_ADD_ID,
        CN_ADD_PositedAt,
        CN_ADD_Positor,
        CN_ADD_Reliability,
        CN_ADD_Reliable
    FROM
        [dbo].[CN_ADD_Contact_Address_Annex]
    WHERE
        CN_ADD_PositedAt <= @positingTimepoint;
    ');
END
GO
-- Attribute assembled rewinder ---------------------------------------------------------------------------------------
-- rCN_ADD_Contact_Address rewinding over changing and positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_ADD_Contact_Address','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_ADD_Contact_Address] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_ADD_ID,
        a.CN_ADD_PositedAt,
        a.CN_ADD_Positor,
        a.CN_ADD_Reliability,
        a.CN_ADD_Reliable,
        p.CN_ADD_CN_ID,
        p.CN_ADD_Contact_Address,
        p.CN_ADD_ChangedAt
    FROM
        [dbo].[rCN_ADD_Contact_Address_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) a
    ON
        a.CN_ADD_ID = p.CN_ADD_ID
    AND
        a.CN_ADD_Positor = @positor
    AND
        a.CN_ADD_PositedAt = (
            SELECT TOP 1
                sub.CN_ADD_PositedAt
            FROM
                [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_ADD_ID = p.CN_ADD_ID
            AND
                sub.CN_ADD_Positor = @positor
            ORDER BY
                sub.CN_ADD_PositedAt DESC
        )
    ');
END
GO
-- Attribute assembled forwarder --------------------------------------------------------------------------------------
-- fCN_ADD_Contact_Address forwarding over changing and rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_ADD_Contact_Address','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_ADD_Contact_Address] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_ADD_ID,
        a.CN_ADD_PositedAt,
        a.CN_ADD_Positor,
        a.CN_ADD_Reliability,
        a.CN_ADD_Reliable,
        p.CN_ADD_CN_ID,
        p.CN_ADD_Contact_Address,
        p.CN_ADD_ChangedAt
    FROM
        [dbo].[fCN_ADD_Contact_Address_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) a
    ON
        a.CN_ADD_ID = p.CN_ADD_ID
    AND
        a.CN_ADD_Positor = @positor
    AND
        a.CN_ADD_PositedAt = (
            SELECT TOP 1
                sub.CN_ADD_PositedAt
            FROM
                [dbo].[rCN_ADD_Contact_Address_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_ADD_ID = p.CN_ADD_ID
            AND
                sub.CN_ADD_Positor = @positor
            ORDER BY
                sub.CN_ADD_PositedAt DESC
        )
    ');
END
GO
-- Attribute previous value -------------------------------------------------------------------------------------------
-- preCN_ADD_Contact_Address function for getting previous value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.preCN_ADD_Contact_Address','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[preCN_ADD_Contact_Address] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(128)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_ADD_Contact_Address
        FROM
            [dbo].[rCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_ADD_CN_ID = @id
        AND
            pre.CN_ADD_ChangedAt < @changingTimepoint
        AND
            pre.CN_ADD_Reliable = 1
        ORDER BY
            pre.CN_ADD_ChangedAt DESC,
            pre.CN_ADD_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute following value ------------------------------------------------------------------------------------------
-- folCN_ADD_Contact_Address function for getting following value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.folCN_ADD_Contact_Address','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[folCN_ADD_Contact_Address] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(128)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_ADD_Contact_Address
        FROM
            [dbo].[fCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_ADD_CN_ID = @id
        AND
            fol.CN_ADD_ChangedAt > @changingTimepoint
        AND
            fol.CN_ADD_Reliable = 1
        ORDER BY
            fol.CN_ADD_ChangedAt ASC,
            fol.CN_ADD_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute posit rewinder -------------------------------------------------------------------------------------------
-- rCN_PHN_Contact_Phone_Posit rewinding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_PHN_Contact_Phone_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_PHN_Contact_Phone_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_PHN_ID,
        CN_PHN_CN_ID,
        CN_PHN_Contact_Phone,
        CN_PHN_ChangedAt
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit]
    WHERE
        CN_PHN_ChangedAt <= @changingTimepoint;
    ');
END
GO
-- Attribute posit forwarder ------------------------------------------------------------------------------------------
-- fCN_PHN_Contact_Phone_Posit forwarding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_PHN_Contact_Phone_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_PHN_Contact_Phone_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_PHN_ID,
        CN_PHN_CN_ID,
        CN_PHN_Contact_Phone,
        CN_PHN_ChangedAt
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit]
    WHERE
        CN_PHN_ChangedAt > @changingTimepoint;
    ');
END
GO
-- Attribute annex rewinder -------------------------------------------------------------------------------------------
-- rCN_PHN_Contact_Phone_Annex rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_PHN_Contact_Phone_Annex','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_PHN_Contact_Phone_Annex] (
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_PHN_ID,
        CN_PHN_PositedAt,
        CN_PHN_Positor,
        CN_PHN_Reliability,
        CN_PHN_Reliable
    FROM
        [dbo].[CN_PHN_Contact_Phone_Annex]
    WHERE
        CN_PHN_PositedAt <= @positingTimepoint;
    ');
END
GO
-- Attribute assembled rewinder ---------------------------------------------------------------------------------------
-- rCN_PHN_Contact_Phone rewinding over changing and positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_PHN_Contact_Phone','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_PHN_Contact_Phone] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_PHN_ID,
        a.CN_PHN_PositedAt,
        a.CN_PHN_Positor,
        a.CN_PHN_Reliability,
        a.CN_PHN_Reliable,
        p.CN_PHN_CN_ID,
        p.CN_PHN_Contact_Phone,
        p.CN_PHN_ChangedAt
    FROM
        [dbo].[rCN_PHN_Contact_Phone_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) a
    ON
        a.CN_PHN_ID = p.CN_PHN_ID
    AND
        a.CN_PHN_Positor = @positor
    AND
        a.CN_PHN_PositedAt = (
            SELECT TOP 1
                sub.CN_PHN_PositedAt
            FROM
                [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_PHN_ID = p.CN_PHN_ID
            AND
                sub.CN_PHN_Positor = @positor
            ORDER BY
                sub.CN_PHN_PositedAt DESC
        )
    ');
END
GO
-- Attribute assembled forwarder --------------------------------------------------------------------------------------
-- fCN_PHN_Contact_Phone forwarding over changing and rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_PHN_Contact_Phone','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_PHN_Contact_Phone] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_PHN_ID,
        a.CN_PHN_PositedAt,
        a.CN_PHN_Positor,
        a.CN_PHN_Reliability,
        a.CN_PHN_Reliable,
        p.CN_PHN_CN_ID,
        p.CN_PHN_Contact_Phone,
        p.CN_PHN_ChangedAt
    FROM
        [dbo].[fCN_PHN_Contact_Phone_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) a
    ON
        a.CN_PHN_ID = p.CN_PHN_ID
    AND
        a.CN_PHN_Positor = @positor
    AND
        a.CN_PHN_PositedAt = (
            SELECT TOP 1
                sub.CN_PHN_PositedAt
            FROM
                [dbo].[rCN_PHN_Contact_Phone_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_PHN_ID = p.CN_PHN_ID
            AND
                sub.CN_PHN_Positor = @positor
            ORDER BY
                sub.CN_PHN_PositedAt DESC
        )
    ');
END
GO
-- Attribute previous value -------------------------------------------------------------------------------------------
-- preCN_PHN_Contact_Phone function for getting previous value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.preCN_PHN_Contact_Phone','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[preCN_PHN_Contact_Phone] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(20)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_PHN_Contact_Phone
        FROM
            [dbo].[rCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_PHN_CN_ID = @id
        AND
            pre.CN_PHN_ChangedAt < @changingTimepoint
        AND
            pre.CN_PHN_Reliable = 1
        ORDER BY
            pre.CN_PHN_ChangedAt DESC,
            pre.CN_PHN_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute following value ------------------------------------------------------------------------------------------
-- folCN_PHN_Contact_Phone function for getting following value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.folCN_PHN_Contact_Phone','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[folCN_PHN_Contact_Phone] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(20)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_PHN_Contact_Phone
        FROM
            [dbo].[fCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_PHN_CN_ID = @id
        AND
            fol.CN_PHN_ChangedAt > @changingTimepoint
        AND
            fol.CN_PHN_Reliable = 1
        ORDER BY
            fol.CN_PHN_ChangedAt ASC,
            fol.CN_PHN_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute posit rewinder -------------------------------------------------------------------------------------------
-- rCN_EML_Contact_EMail_Posit rewinding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_EML_Contact_EMail_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_EML_Contact_EMail_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_EML_ID,
        CN_EML_CN_ID,
        CN_EML_Contact_EMail,
        CN_EML_ChangedAt
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit]
    WHERE
        CN_EML_ChangedAt <= @changingTimepoint;
    ');
END
GO
-- Attribute posit forwarder ------------------------------------------------------------------------------------------
-- fCN_EML_Contact_EMail_Posit forwarding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_EML_Contact_EMail_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_EML_Contact_EMail_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_EML_ID,
        CN_EML_CN_ID,
        CN_EML_Contact_EMail,
        CN_EML_ChangedAt
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit]
    WHERE
        CN_EML_ChangedAt > @changingTimepoint;
    ');
END
GO
-- Attribute annex rewinder -------------------------------------------------------------------------------------------
-- rCN_EML_Contact_EMail_Annex rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_EML_Contact_EMail_Annex','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_EML_Contact_EMail_Annex] (
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        CN_EML_ID,
        CN_EML_PositedAt,
        CN_EML_Positor,
        CN_EML_Reliability,
        CN_EML_Reliable
    FROM
        [dbo].[CN_EML_Contact_EMail_Annex]
    WHERE
        CN_EML_PositedAt <= @positingTimepoint;
    ');
END
GO
-- Attribute assembled rewinder ---------------------------------------------------------------------------------------
-- rCN_EML_Contact_EMail rewinding over changing and positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rCN_EML_Contact_EMail','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rCN_EML_Contact_EMail] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_EML_ID,
        a.CN_EML_PositedAt,
        a.CN_EML_Positor,
        a.CN_EML_Reliability,
        a.CN_EML_Reliable,
        p.CN_EML_CN_ID,
        p.CN_EML_Contact_EMail,
        p.CN_EML_ChangedAt
    FROM
        [dbo].[rCN_EML_Contact_EMail_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) a
    ON
        a.CN_EML_ID = p.CN_EML_ID
    AND
        a.CN_EML_Positor = @positor
    AND
        a.CN_EML_PositedAt = (
            SELECT TOP 1
                sub.CN_EML_PositedAt
            FROM
                [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_EML_ID = p.CN_EML_ID
            AND
                sub.CN_EML_Positor = @positor
            ORDER BY
                sub.CN_EML_PositedAt DESC
        )
    ');
END
GO
-- Attribute assembled forwarder --------------------------------------------------------------------------------------
-- fCN_EML_Contact_EMail forwarding over changing and rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fCN_EML_Contact_EMail','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fCN_EML_Contact_EMail] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.CN_EML_ID,
        a.CN_EML_PositedAt,
        a.CN_EML_Positor,
        a.CN_EML_Reliability,
        a.CN_EML_Reliable,
        p.CN_EML_CN_ID,
        p.CN_EML_Contact_EMail,
        p.CN_EML_ChangedAt
    FROM
        [dbo].[fCN_EML_Contact_EMail_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) a
    ON
        a.CN_EML_ID = p.CN_EML_ID
    AND
        a.CN_EML_Positor = @positor
    AND
        a.CN_EML_PositedAt = (
            SELECT TOP 1
                sub.CN_EML_PositedAt
            FROM
                [dbo].[rCN_EML_Contact_EMail_Annex](@positingTimepoint) sub
            WHERE
                sub.CN_EML_ID = p.CN_EML_ID
            AND
                sub.CN_EML_Positor = @positor
            ORDER BY
                sub.CN_EML_PositedAt DESC
        )
    ');
END
GO
-- Attribute previous value -------------------------------------------------------------------------------------------
-- preCN_EML_Contact_EMail function for getting previous value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.preCN_EML_Contact_EMail','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[preCN_EML_Contact_EMail] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(254)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.CN_EML_Contact_EMail
        FROM
            [dbo].[rCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.CN_EML_CN_ID = @id
        AND
            pre.CN_EML_ChangedAt < @changingTimepoint
        AND
            pre.CN_EML_Reliable = 1
        ORDER BY
            pre.CN_EML_ChangedAt DESC,
            pre.CN_EML_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute following value ------------------------------------------------------------------------------------------
-- folCN_EML_Contact_EMail function for getting following value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.folCN_EML_Contact_EMail','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[folCN_EML_Contact_EMail] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(254)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.CN_EML_Contact_EMail
        FROM
            [dbo].[fCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.CN_EML_CN_ID = @id
        AND
            fol.CN_EML_ChangedAt > @changingTimepoint
        AND
            fol.CN_EML_Reliable = 1
        ORDER BY
            fol.CN_EML_ChangedAt ASC,
            fol.CN_EML_PositedAt DESC
    );
    END
    ');
END
GO
-- ATTRIBUTE RESTATEMENT CONSTRAINTS ----------------------------------------------------------------------------------
--
-- Attributes may be prevented from storing restatements.
-- A restatement is when the same value occurs for two adjacent points
-- in changing time. Note that restatement checking is not done for
-- unreliable information as this could prevent demotion.
--
-- returns 1 for at least one equal surrounding value, 0 for different surrounding values
--
-- @posit the id of the posit that should be checked
-- @posited the time when this posit was made
-- @positor the one who made the posit
-- @reliable whether this posit is considered reliable (1) or unreliable (0)
--
-- Restatement Finder Function and Constraint -------------------------------------------------------------------------
-- rfCN_ADD_Contact_Address restatement finder, also used by the insert and update triggers for idempotent attributes
-- rcCN_ADD_Contact_Address restatement constraint (available only in attributes that cannot have restatements)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rfCN_ADD_Contact_Address', 'FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rfCN_ADD_Contact_Address] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(128);
    DECLARE @changed datetime;
    SELECT
        @id = CN_ADD_CN_ID,
        @value = CN_ADD_Contact_Address,
        @changed = CN_ADD_ChangedAt
    FROM
        [dbo].[CN_ADD_Contact_Address_Posit]
    WHERE
        CN_ADD_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_ADD_Contact_Address
                    FROM
                        [dbo].[rCN_ADD_Contact_Address](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_ADD_CN_ID = @id
                    AND
                        pre.CN_ADD_ChangedAt < @changed
                    AND
                        pre.CN_ADD_Reliable = 1
                    ORDER BY
                        pre.CN_ADD_ChangedAt DESC,
                        pre.CN_ADD_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_ADD_Contact_Address
                    FROM
                        [dbo].[fCN_ADD_Contact_Address](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_ADD_CN_ID = @id
                    AND
                        fol.CN_ADD_ChangedAt > @changed
                    AND
                        fol.CN_ADD_Reliable = 1
                    ORDER BY
                        fol.CN_ADD_ChangedAt ASC,
                        fol.CN_ADD_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    ');
    ALTER TABLE [dbo].[CN_ADD_Contact_Address_Annex]
    ADD CONSTRAINT [rcCN_ADD_Contact_Address_Annex] CHECK (
        [dbo].[rfCN_ADD_Contact_Address] (
            CN_ADD_ID,
            CN_ADD_PositedAt,
            CN_ADD_Positor,
            CN_ADD_Reliable
        ) = 0
    );
END
GO
-- Restatement Finder Function and Constraint -------------------------------------------------------------------------
-- rfCN_PHN_Contact_Phone restatement finder, also used by the insert and update triggers for idempotent attributes
-- rcCN_PHN_Contact_Phone restatement constraint (available only in attributes that cannot have restatements)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rfCN_PHN_Contact_Phone', 'FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rfCN_PHN_Contact_Phone] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(20);
    DECLARE @changed datetime;
    SELECT
        @id = CN_PHN_CN_ID,
        @value = CN_PHN_Contact_Phone,
        @changed = CN_PHN_ChangedAt
    FROM
        [dbo].[CN_PHN_Contact_Phone_Posit]
    WHERE
        CN_PHN_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_PHN_Contact_Phone
                    FROM
                        [dbo].[rCN_PHN_Contact_Phone](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_PHN_CN_ID = @id
                    AND
                        pre.CN_PHN_ChangedAt < @changed
                    AND
                        pre.CN_PHN_Reliable = 1
                    ORDER BY
                        pre.CN_PHN_ChangedAt DESC,
                        pre.CN_PHN_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_PHN_Contact_Phone
                    FROM
                        [dbo].[fCN_PHN_Contact_Phone](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_PHN_CN_ID = @id
                    AND
                        fol.CN_PHN_ChangedAt > @changed
                    AND
                        fol.CN_PHN_Reliable = 1
                    ORDER BY
                        fol.CN_PHN_ChangedAt ASC,
                        fol.CN_PHN_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    ');
    ALTER TABLE [dbo].[CN_PHN_Contact_Phone_Annex]
    ADD CONSTRAINT [rcCN_PHN_Contact_Phone_Annex] CHECK (
        [dbo].[rfCN_PHN_Contact_Phone] (
            CN_PHN_ID,
            CN_PHN_PositedAt,
            CN_PHN_Positor,
            CN_PHN_Reliable
        ) = 0
    );
END
GO
-- Restatement Finder Function and Constraint -------------------------------------------------------------------------
-- rfCN_EML_Contact_EMail restatement finder, also used by the insert and update triggers for idempotent attributes
-- rcCN_EML_Contact_EMail restatement constraint (available only in attributes that cannot have restatements)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rfCN_EML_Contact_EMail', 'FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rfCN_EML_Contact_EMail] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(254);
    DECLARE @changed datetime;
    SELECT
        @id = CN_EML_CN_ID,
        @value = CN_EML_Contact_EMail,
        @changed = CN_EML_ChangedAt
    FROM
        [dbo].[CN_EML_Contact_EMail_Posit]
    WHERE
        CN_EML_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.CN_EML_Contact_EMail
                    FROM
                        [dbo].[rCN_EML_Contact_EMail](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.CN_EML_CN_ID = @id
                    AND
                        pre.CN_EML_ChangedAt < @changed
                    AND
                        pre.CN_EML_Reliable = 1
                    ORDER BY
                        pre.CN_EML_ChangedAt DESC,
                        pre.CN_EML_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.CN_EML_Contact_EMail
                    FROM
                        [dbo].[fCN_EML_Contact_EMail](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.CN_EML_CN_ID = @id
                    AND
                        fol.CN_EML_ChangedAt > @changed
                    AND
                        fol.CN_EML_Reliable = 1
                    ORDER BY
                        fol.CN_EML_ChangedAt ASC,
                        fol.CN_EML_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    ');
    ALTER TABLE [dbo].[CN_EML_Contact_EMail_Annex]
    ADD CONSTRAINT [rcCN_EML_Contact_EMail_Annex] CHECK (
        [dbo].[rfCN_EML_Contact_EMail] (
            CN_EML_ID,
            CN_EML_PositedAt,
            CN_EML_Positor,
            CN_EML_Reliable
        ) = 0
    );
END
GO
-- ANCHOR TEMPORAL PERSPECTIVES ---------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each anchor. There are five types of perspectives: time traveling, latest,
-- point-in-time, difference, and now. They also denormalize the anchor, its attributes,
-- and referenced knots from sixth to third normal form.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @reliable whether to show reliable (1) or unreliable (0) results
--
-- The latest perspective shows the latest available (changing & positing) information for each anchor.
-- The now perspective shows the information as it is right now, with latest positing time.
-- The point-in-time perspective lets you travel through the information to the given timepoint,
-- with latest positing time and the given point in changing time.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints, and for
-- changes in all or a selection of attributes, with latest positing time.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
-- @selection a list of mnemonics for tracked attributes, ie 'MNE MON ICS', or null for all
--
-- Drop perspectives --------------------------------------------------------------------------------------------------
IF Object_ID('dbo.dCN_Contact', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[dCN_Contact];
IF Object_ID('dbo.nCN_Contact', 'V') IS NOT NULL
DROP VIEW [dbo].[nCN_Contact];
IF Object_ID('dbo.pCN_Contact', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[pCN_Contact];
IF Object_ID('dbo.lCN_Contact', 'V') IS NOT NULL
DROP VIEW [dbo].[lCN_Contact];
IF Object_ID('dbo.tCN_Contact', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[tCN_Contact];
GO
-- Time traveling perspective -----------------------------------------------------------------------------------------
-- tCN_Contact viewed as given by the input parameters
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[tCN_Contact] (
    @positor tinyint = 0,
    @changingTimepoint datetime2(7) = '9999-12-31',
    @positingTimepoint datetime = '9999-12-31',
    @reliable tinyint = 1
)
RETURNS TABLE WITH SCHEMABINDING AS RETURN
SELECT
    [CN].CN_ID,
    [ADD].CN_ADD_CN_ID,
    [ADD].CN_ADD_ID,
    [ADD].CN_ADD_ChangedAt,
    [ADD].CN_ADD_PositedAt,
    [ADD].CN_ADD_Positor,
    [ADD].CN_ADD_Reliability,
    [ADD].CN_ADD_Reliable,
    [ADD].CN_ADD_Contact_Address,
    [PHN].CN_PHN_CN_ID,
    [PHN].CN_PHN_ID,
    [PHN].CN_PHN_ChangedAt,
    [PHN].CN_PHN_PositedAt,
    [PHN].CN_PHN_Positor,
    [PHN].CN_PHN_Reliability,
    [PHN].CN_PHN_Reliable,
    [PHN].CN_PHN_Contact_Phone,
    [EML].CN_EML_CN_ID,
    [EML].CN_EML_ID,
    [EML].CN_EML_ChangedAt,
    [EML].CN_EML_PositedAt,
    [EML].CN_EML_Positor,
    [EML].CN_EML_Reliability,
    [EML].CN_EML_Reliable,
    [EML].CN_EML_Contact_EMail
FROM
    [dbo].[CN_Contact] [CN]
LEFT JOIN
    [dbo].[rCN_ADD_Contact_Address](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [ADD]
ON
    [ADD].CN_ADD_ID = (
        SELECT TOP 1
            sub.CN_ADD_ID
        FROM
            [dbo].[rCN_ADD_Contact_Address](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_ADD_CN_ID = [CN].CN_ID
        AND
            sub.CN_ADD_Reliable = @reliable
        ORDER BY
            sub.CN_ADD_ChangedAt DESC,
            sub.CN_ADD_PositedAt DESC
    )
LEFT JOIN
    [dbo].[rCN_PHN_Contact_Phone](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [PHN]
ON
    [PHN].CN_PHN_ID = (
        SELECT TOP 1
            sub.CN_PHN_ID
        FROM
            [dbo].[rCN_PHN_Contact_Phone](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_PHN_CN_ID = [CN].CN_ID
        AND
            sub.CN_PHN_Reliable = @reliable
        ORDER BY
            sub.CN_PHN_ChangedAt DESC,
            sub.CN_PHN_PositedAt DESC
    )
LEFT JOIN
    [dbo].[rCN_EML_Contact_EMail](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [EML]
ON
    [EML].CN_EML_ID = (
        SELECT TOP 1
            sub.CN_EML_ID
        FROM
            [dbo].[rCN_EML_Contact_EMail](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.CN_EML_CN_ID = [CN].CN_ID
        AND
            sub.CN_EML_Reliable = @reliable
        ORDER BY
            sub.CN_EML_ChangedAt DESC,
            sub.CN_EML_PositedAt DESC
    );
GO
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lCN_Contact viewed by the latest available information for all positors (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[lCN_Contact]
AS
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        DEFAULT,
        DEFAULT,
        DEFAULT
    ) [CN];
GO
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pCN_Contact viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[pCN_Contact] (
    @changingTimepoint datetime2(7)
)
RETURNS TABLE AS RETURN
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        @changingTimepoint,
        DEFAULT,
        DEFAULT
    ) [CN];
GO
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nCN_Contact viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[nCN_Contact]
AS
SELECT
    p.*, 
    1 as Reliable,
    [CN].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tCN_Contact] (
        p.Positor,
        sysdatetime(),
        DEFAULT,
        DEFAULT
    ) [CN];
GO
-- Difference perspective ---------------------------------------------------------------------------------------------
-- dCN_Contact showing all differences between the given timepoints and optionally for a subset of attributes
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[dCN_Contact] (
    @intervalStart datetime2(7),
    @intervalEnd datetime2(7),
    @selection varchar(max) = null
)
RETURNS TABLE AS RETURN
SELECT
    timepoints.inspectedTimepoint,
    [CN].*
FROM
    [dbo].[_Positor] p
JOIN (
    SELECT DISTINCT
        CN_ADD_Positor AS positor,
        CN_ADD_CN_ID AS CN_ID,
        CN_ADD_ChangedAt AS inspectedTimepoint,
        'ADD' AS mnemonic
    FROM
        [dbo].[CN_ADD_Contact_Address]
    WHERE
        (@selection is null OR @selection like '%ADD%')
    AND
        CN_ADD_ChangedAt BETWEEN @intervalStart AND @intervalEnd
    UNION
    SELECT DISTINCT
        CN_PHN_Positor AS positor,
        CN_PHN_CN_ID AS CN_ID,
        CN_PHN_ChangedAt AS inspectedTimepoint,
        'PHN' AS mnemonic
    FROM
        [dbo].[CN_PHN_Contact_Phone]
    WHERE
        (@selection is null OR @selection like '%PHN%')
    AND
        CN_PHN_ChangedAt BETWEEN @intervalStart AND @intervalEnd
    UNION
    SELECT DISTINCT
        CN_EML_Positor AS positor,
        CN_EML_CN_ID AS CN_ID,
        CN_EML_ChangedAt AS inspectedTimepoint,
        'EML' AS mnemonic
    FROM
        [dbo].[CN_EML_Contact_EMail]
    WHERE
        (@selection is null OR @selection like '%EML%')
    AND
        CN_EML_ChangedAt BETWEEN @intervalStart AND @intervalEnd
) timepoints
ON
    timepoints.positor = p.Positor
CROSS APPLY
    [dbo].[tCN_Contact] (
        timepoints.positor,
        timepoints.inspectedTimepoint,
        DEFAULT,
        DEFAULT
    ) [CN]
 WHERE
    [CN].CN_ID = timepoints.CN_ID;
GO
-- KEY GENERATORS -----------------------------------------------------------------------------------------------------
--
-- These stored procedures can be used to generate identities of entities.
-- Corresponding anchors must have an incrementing identity column.
--
-- Key Generation Stored Procedure ------------------------------------------------------------------------------------
-- kCN_Contact identity by surrogate key generation stored procedure
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.kCN_Contact', 'P') IS NULL
BEGIN
    EXEC('
    CREATE PROCEDURE [dbo].[kCN_Contact] (
        @requestedNumberOfIdentities bigint
    ) AS
    BEGIN
        SET NOCOUNT ON;
        IF @requestedNumberOfIdentities > 0
        BEGIN
            WITH idGenerator (idNumber) AS (
                SELECT
                    1
                UNION ALL
                SELECT
                    idNumber + 1
                FROM
                    idGenerator
                WHERE
                    idNumber < @requestedNumberOfIdentities
            )
            INSERT INTO [dbo].[CN_Contact] (
                CN_Dummy
            )
            OUTPUT
                inserted.CN_ID
            SELECT
                null
            FROM
                idGenerator
            OPTION (maxrecursion 0);
        END
    END
    ');
END
GO
-- ATTRIBUTE TRIGGERS ------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled views make them behave like tables.
-- There is one 'instead of' trigger for: insert.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_CN_ADD_Contact_Address instead of INSERT trigger on CN_ADD_Contact_Address
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.it_CN_ADD_Contact_Address', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[it_CN_ADD_Contact_Address];
GO
CREATE TRIGGER [dbo].[it_CN_ADD_Contact_Address] ON [dbo].[CN_ADD_Contact_Address]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_ADD_Contact_Address TABLE (
        CN_ADD_CN_ID int not null,
        CN_ADD_ChangedAt datetime not null,
        CN_ADD_Positor tinyint not null,
        CN_ADD_PositedAt datetime not null,
        CN_ADD_Reliability tinyint not null,
        CN_ADD_Reliable tinyint not null,
        CN_ADD_Contact_Address VARCHAR(128) not null,
        CN_ADD_Version bigint not null,
        CN_ADD_StatementType char(1) not null,
        primary key(
            CN_ADD_Version,
            CN_ADD_Positor,
            CN_ADD_CN_ID
        )
    );
    INSERT INTO @CN_ADD_Contact_Address
    SELECT
        i.CN_ADD_CN_ID,
        i.CN_ADD_ChangedAt,
        i.CN_ADD_Positor,
        i.CN_ADD_PositedAt,
        i.CN_ADD_Reliability,
        case
            when i.CN_ADD_Reliability < 1 then 0
            else 1
        end,
        i.CN_ADD_Contact_Address,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_ADD_Positor,
                i.CN_ADD_CN_ID
            ORDER BY
                i.CN_ADD_ChangedAt ASC,
                i.CN_ADD_PositedAt ASC,
                i.CN_ADD_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_ADD_Version),
        @currentVersion = 0
    FROM
        @CN_ADD_Contact_Address;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_ADD_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_ADD_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_ADD_Positor, v.CN_ADD_ChangedAt, v.CN_ADD_PositedAt, v.CN_ADD_Reliable) t
                        WHERE
                            t.CN_ADD_CN_ID = v.CN_ADD_CN_ID
                        AND
                            t.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
                        AND
                            t.CN_ADD_Reliability = v.CN_ADD_Reliability
                        AND
                            t.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_ADD_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_ADD_Contact_Address
                        WHERE
                            v.CN_ADD_Contact_Address =
                                dbo.preCN_ADD_Contact_Address (
                                    v.CN_ADD_CN_ID,
                                    v.CN_ADD_Positor,
                                    v.CN_ADD_ChangedAt,
                                    v.CN_ADD_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_ADD_Contact_Address
                        WHERE
                            v.CN_ADD_Contact_Address =
                                dbo.folCN_ADD_Contact_Address (
                                    v.CN_ADD_CN_ID,
                                    v.CN_ADD_Positor,
                                    v.CN_ADD_ChangedAt,
                                    v.CN_ADD_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_ADD_Contact_Address v
        LEFT JOIN
            [dbo].[CN_ADD_Contact_Address_Posit] p
        ON
            p.CN_ADD_CN_ID = v.CN_ADD_CN_ID
        AND
            p.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
        AND
            p.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
        WHERE
            v.CN_ADD_Version = @currentVersion;
        INSERT INTO [dbo].[CN_ADD_Contact_Address_Posit] (
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Contact_Address
        )
        SELECT
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Contact_Address
        FROM
            @CN_ADD_Contact_Address
        WHERE
            CN_ADD_Version = @currentVersion
        AND
            CN_ADD_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_ADD_Contact_Address_Annex] (
            CN_ADD_ID,
            CN_ADD_Positor,
            CN_ADD_PositedAt,
            CN_ADD_Reliability
        )
        SELECT
            p.CN_ADD_ID,
            v.CN_ADD_Positor,
            v.CN_ADD_PositedAt,
            v.CN_ADD_Reliability
        FROM
            @CN_ADD_Contact_Address v
        JOIN
            [dbo].[CN_ADD_Contact_Address_Posit] p
        ON
            p.CN_ADD_CN_ID = v.CN_ADD_CN_ID
        AND
            p.CN_ADD_ChangedAt = v.CN_ADD_ChangedAt
        AND
            p.CN_ADD_Contact_Address = v.CN_ADD_Contact_Address
        WHERE
            v.CN_ADD_Version = @currentVersion
        AND
            CN_ADD_StatementType in ('S','N','D','R');
    END
END
GO
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_CN_PHN_Contact_Phone instead of INSERT trigger on CN_PHN_Contact_Phone
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.it_CN_PHN_Contact_Phone', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[it_CN_PHN_Contact_Phone];
GO
CREATE TRIGGER [dbo].[it_CN_PHN_Contact_Phone] ON [dbo].[CN_PHN_Contact_Phone]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_PHN_Contact_Phone TABLE (
        CN_PHN_CN_ID int not null,
        CN_PHN_ChangedAt datetime not null,
        CN_PHN_Positor tinyint not null,
        CN_PHN_PositedAt datetime not null,
        CN_PHN_Reliability tinyint not null,
        CN_PHN_Reliable tinyint not null,
        CN_PHN_Contact_Phone VARCHAR(20) not null,
        CN_PHN_Version bigint not null,
        CN_PHN_StatementType char(1) not null,
        primary key(
            CN_PHN_Version,
            CN_PHN_Positor,
            CN_PHN_CN_ID
        )
    );
    INSERT INTO @CN_PHN_Contact_Phone
    SELECT
        i.CN_PHN_CN_ID,
        i.CN_PHN_ChangedAt,
        i.CN_PHN_Positor,
        i.CN_PHN_PositedAt,
        i.CN_PHN_Reliability,
        case
            when i.CN_PHN_Reliability < 1 then 0
            else 1
        end,
        i.CN_PHN_Contact_Phone,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_PHN_Positor,
                i.CN_PHN_CN_ID
            ORDER BY
                i.CN_PHN_ChangedAt ASC,
                i.CN_PHN_PositedAt ASC,
                i.CN_PHN_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_PHN_Version),
        @currentVersion = 0
    FROM
        @CN_PHN_Contact_Phone;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_PHN_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_PHN_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_PHN_Positor, v.CN_PHN_ChangedAt, v.CN_PHN_PositedAt, v.CN_PHN_Reliable) t
                        WHERE
                            t.CN_PHN_CN_ID = v.CN_PHN_CN_ID
                        AND
                            t.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
                        AND
                            t.CN_PHN_Reliability = v.CN_PHN_Reliability
                        AND
                            t.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_PHN_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_PHN_Contact_Phone
                        WHERE
                            v.CN_PHN_Contact_Phone =
                                dbo.preCN_PHN_Contact_Phone (
                                    v.CN_PHN_CN_ID,
                                    v.CN_PHN_Positor,
                                    v.CN_PHN_ChangedAt,
                                    v.CN_PHN_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_PHN_Contact_Phone
                        WHERE
                            v.CN_PHN_Contact_Phone =
                                dbo.folCN_PHN_Contact_Phone (
                                    v.CN_PHN_CN_ID,
                                    v.CN_PHN_Positor,
                                    v.CN_PHN_ChangedAt,
                                    v.CN_PHN_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_PHN_Contact_Phone v
        LEFT JOIN
            [dbo].[CN_PHN_Contact_Phone_Posit] p
        ON
            p.CN_PHN_CN_ID = v.CN_PHN_CN_ID
        AND
            p.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
        AND
            p.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
        WHERE
            v.CN_PHN_Version = @currentVersion;
        INSERT INTO [dbo].[CN_PHN_Contact_Phone_Posit] (
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Contact_Phone
        )
        SELECT
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Contact_Phone
        FROM
            @CN_PHN_Contact_Phone
        WHERE
            CN_PHN_Version = @currentVersion
        AND
            CN_PHN_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_PHN_Contact_Phone_Annex] (
            CN_PHN_ID,
            CN_PHN_Positor,
            CN_PHN_PositedAt,
            CN_PHN_Reliability
        )
        SELECT
            p.CN_PHN_ID,
            v.CN_PHN_Positor,
            v.CN_PHN_PositedAt,
            v.CN_PHN_Reliability
        FROM
            @CN_PHN_Contact_Phone v
        JOIN
            [dbo].[CN_PHN_Contact_Phone_Posit] p
        ON
            p.CN_PHN_CN_ID = v.CN_PHN_CN_ID
        AND
            p.CN_PHN_ChangedAt = v.CN_PHN_ChangedAt
        AND
            p.CN_PHN_Contact_Phone = v.CN_PHN_Contact_Phone
        WHERE
            v.CN_PHN_Version = @currentVersion
        AND
            CN_PHN_StatementType in ('S','N','D','R');
    END
END
GO
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_CN_EML_Contact_EMail instead of INSERT trigger on CN_EML_Contact_EMail
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.it_CN_EML_Contact_EMail', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[it_CN_EML_Contact_EMail];
GO
CREATE TRIGGER [dbo].[it_CN_EML_Contact_EMail] ON [dbo].[CN_EML_Contact_EMail]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @CN_EML_Contact_EMail TABLE (
        CN_EML_CN_ID int not null,
        CN_EML_ChangedAt datetime not null,
        CN_EML_Positor tinyint not null,
        CN_EML_PositedAt datetime not null,
        CN_EML_Reliability tinyint not null,
        CN_EML_Reliable tinyint not null,
        CN_EML_Contact_EMail VARCHAR(254) not null,
        CN_EML_Version bigint not null,
        CN_EML_StatementType char(1) not null,
        primary key(
            CN_EML_Version,
            CN_EML_Positor,
            CN_EML_CN_ID
        )
    );
    INSERT INTO @CN_EML_Contact_EMail
    SELECT
        i.CN_EML_CN_ID,
        i.CN_EML_ChangedAt,
        i.CN_EML_Positor,
        i.CN_EML_PositedAt,
        i.CN_EML_Reliability,
        case
            when i.CN_EML_Reliability < 1 then 0
            else 1
        end,
        i.CN_EML_Contact_EMail,
        DENSE_RANK() OVER (
            PARTITION BY
                i.CN_EML_Positor,
                i.CN_EML_CN_ID
            ORDER BY
                i.CN_EML_ChangedAt ASC,
                i.CN_EML_PositedAt ASC,
                i.CN_EML_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(CN_EML_Version),
        @currentVersion = 0
    FROM
        @CN_EML_Contact_EMail;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.CN_EML_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.CN_EML_ID
                        FROM
                            [dbo].[tCN_Contact](v.CN_EML_Positor, v.CN_EML_ChangedAt, v.CN_EML_PositedAt, v.CN_EML_Reliable) t
                        WHERE
                            t.CN_EML_CN_ID = v.CN_EML_CN_ID
                        AND
                            t.CN_EML_ChangedAt = v.CN_EML_ChangedAt
                        AND
                            t.CN_EML_Reliability = v.CN_EML_Reliability
                        AND
                            t.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.CN_EML_CN_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.CN_EML_Contact_EMail
                        WHERE
                            v.CN_EML_Contact_EMail =
                                dbo.preCN_EML_Contact_EMail (
                                    v.CN_EML_CN_ID,
                                    v.CN_EML_Positor,
                                    v.CN_EML_ChangedAt,
                                    v.CN_EML_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.CN_EML_Contact_EMail
                        WHERE
                            v.CN_EML_Contact_EMail =
                                dbo.folCN_EML_Contact_EMail (
                                    v.CN_EML_CN_ID,
                                    v.CN_EML_Positor,
                                    v.CN_EML_ChangedAt,
                                    v.CN_EML_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @CN_EML_Contact_EMail v
        LEFT JOIN
            [dbo].[CN_EML_Contact_EMail_Posit] p
        ON
            p.CN_EML_CN_ID = v.CN_EML_CN_ID
        AND
            p.CN_EML_ChangedAt = v.CN_EML_ChangedAt
        AND
            p.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
        WHERE
            v.CN_EML_Version = @currentVersion;
        INSERT INTO [dbo].[CN_EML_Contact_EMail_Posit] (
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Contact_EMail
        )
        SELECT
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Contact_EMail
        FROM
            @CN_EML_Contact_EMail
        WHERE
            CN_EML_Version = @currentVersion
        AND
            CN_EML_StatementType in ('N','D','R');
        INSERT INTO [dbo].[CN_EML_Contact_EMail_Annex] (
            CN_EML_ID,
            CN_EML_Positor,
            CN_EML_PositedAt,
            CN_EML_Reliability
        )
        SELECT
            p.CN_EML_ID,
            v.CN_EML_Positor,
            v.CN_EML_PositedAt,
            v.CN_EML_Reliability
        FROM
            @CN_EML_Contact_EMail v
        JOIN
            [dbo].[CN_EML_Contact_EMail_Posit] p
        ON
            p.CN_EML_CN_ID = v.CN_EML_CN_ID
        AND
            p.CN_EML_ChangedAt = v.CN_EML_ChangedAt
        AND
            p.CN_EML_Contact_EMail = v.CN_EML_Contact_EMail
        WHERE
            v.CN_EML_Version = @currentVersion
        AND
            CN_EML_StatementType in ('S','N','D','R');
    END
END
GO
-- ANCHOR TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers on the latest view make it behave like a table.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_lCN_Contact instead of INSERT trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[it_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    DECLARE @CN TABLE (
        Row bigint IDENTITY(1,1) not null primary key,
        CN_ID int not null
    );
    INSERT INTO [dbo].[CN_Contact] (
        CN_Dummy
    )
    OUTPUT
        inserted.CN_ID
    INTO
        @CN
    SELECT
        null
    FROM
        inserted
    WHERE
        inserted.CN_ID is null;
    DECLARE @inserted TABLE (
        CN_ID int not null,
        CN_ADD_CN_ID int null,
        CN_ADD_ChangedAt datetime null,
        CN_ADD_Positor tinyint null,
        CN_ADD_PositedAt datetime null,
        CN_ADD_Reliability tinyint null,
        CN_ADD_Contact_Address VARCHAR(128) null,
        CN_PHN_CN_ID int null,
        CN_PHN_ChangedAt datetime null,
        CN_PHN_Positor tinyint null,
        CN_PHN_PositedAt datetime null,
        CN_PHN_Reliability tinyint null,
        CN_PHN_Contact_Phone VARCHAR(20) null,
        CN_EML_CN_ID int null,
        CN_EML_ChangedAt datetime null,
        CN_EML_Positor tinyint null,
        CN_EML_PositedAt datetime null,
        CN_EML_Reliability tinyint null,
        CN_EML_Contact_EMail VARCHAR(254) null
    );
    INSERT INTO @inserted
    SELECT
        ISNULL(i.CN_ID, a.CN_ID),
        ISNULL(ISNULL(i.CN_ADD_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_ADD_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_ADD_Positor, i.Positor), 0),
        ISNULL(i.CN_ADD_PositedAt, @now),
        ISNULL(i.CN_ADD_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_ADD_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_ADD_Contact_Address,
        ISNULL(ISNULL(i.CN_PHN_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_PHN_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_PHN_Positor, i.Positor), 0),
        ISNULL(i.CN_PHN_PositedAt, @now),
        ISNULL(i.CN_PHN_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_PHN_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_PHN_Contact_Phone,
        ISNULL(ISNULL(i.CN_EML_CN_ID, i.CN_ID), a.CN_ID),
        ISNULL(i.CN_EML_ChangedAt, @now),
        ISNULL(ISNULL(i.CN_EML_Positor, i.Positor), 0),
        ISNULL(i.CN_EML_PositedAt, @now),
        ISNULL(i.CN_EML_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.CN_EML_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.CN_EML_Contact_EMail
    FROM (
        SELECT
            Positor,
            Reliable,
            CN_ID,
            CN_ADD_CN_ID,
            CN_ADD_ChangedAt,
            CN_ADD_Positor,
            CN_ADD_PositedAt,
            CN_ADD_Reliability,
            CN_ADD_Reliable,
            CN_ADD_Contact_Address,
            CN_PHN_CN_ID,
            CN_PHN_ChangedAt,
            CN_PHN_Positor,
            CN_PHN_PositedAt,
            CN_PHN_Reliability,
            CN_PHN_Reliable,
            CN_PHN_Contact_Phone,
            CN_EML_CN_ID,
            CN_EML_ChangedAt,
            CN_EML_Positor,
            CN_EML_PositedAt,
            CN_EML_Reliability,
            CN_EML_Reliable,
            CN_EML_Contact_EMail,
            ROW_NUMBER() OVER (PARTITION BY CN_ID ORDER BY CN_ID) AS Row
        FROM
            inserted
    ) i
    LEFT JOIN
        @CN a
    ON
        a.Row = i.Row;
    INSERT INTO [dbo].[CN_ADD_Contact_Address] (
        CN_ADD_CN_ID,
        CN_ADD_Contact_Address,
        CN_ADD_ChangedAt,
        CN_ADD_PositedAt,
        CN_ADD_Positor,
        CN_ADD_Reliability
    )
    SELECT
        i.CN_ADD_CN_ID,
        i.CN_ADD_Contact_Address,
        i.CN_ADD_ChangedAt,
        i.CN_ADD_PositedAt,
        i.CN_ADD_Positor,
        i.CN_ADD_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_ADD_Contact_Address is not null;
    INSERT INTO [dbo].[CN_PHN_Contact_Phone] (
        CN_PHN_CN_ID,
        CN_PHN_Contact_Phone,
        CN_PHN_ChangedAt,
        CN_PHN_PositedAt,
        CN_PHN_Positor,
        CN_PHN_Reliability
    )
    SELECT
        i.CN_PHN_CN_ID,
        i.CN_PHN_Contact_Phone,
        i.CN_PHN_ChangedAt,
        i.CN_PHN_PositedAt,
        i.CN_PHN_Positor,
        i.CN_PHN_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_PHN_Contact_Phone is not null;
    INSERT INTO [dbo].[CN_EML_Contact_EMail] (
        CN_EML_CN_ID,
        CN_EML_Contact_EMail,
        CN_EML_ChangedAt,
        CN_EML_PositedAt,
        CN_EML_Positor,
        CN_EML_Reliability
    )
    SELECT
        i.CN_EML_CN_ID,
        i.CN_EML_Contact_EMail,
        i.CN_EML_ChangedAt,
        i.CN_EML_PositedAt,
        i.CN_EML_Positor,
        i.CN_EML_Reliability
    FROM
        @inserted i
    WHERE
        i.CN_EML_Contact_EMail is not null;
END
GO
-- UPDATE trigger -----------------------------------------------------------------------------------------------------
-- ut_lCN_Contact instead of UPDATE trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[ut_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    IF(UPDATE(CN_ID))
        RAISERROR('The identity column CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_ID))
        RAISERROR('The identity column CN_ADD_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_CN_ID))
        RAISERROR('The foreign key column CN_ADD_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_ADD_Contact_Address))
    BEGIN
        INSERT INTO [dbo].[CN_ADD_Contact_Address] (
            CN_ADD_CN_ID,
            CN_ADD_Contact_Address,
            CN_ADD_ChangedAt,
            CN_ADD_PositedAt,
            CN_ADD_Positor,
            CN_ADD_Reliability
        )
        SELECT
            ISNULL(i.CN_ADD_CN_ID, i.CN_ID),
            i.CN_ADD_Contact_Address,
            cast(CASE
                WHEN i.CN_ADD_Contact_Address is null THEN i.CN_ADD_ChangedAt
                WHEN UPDATE(CN_ADD_ChangedAt) THEN i.CN_ADD_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_ADD_PositedAt) THEN i.CN_ADD_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_ADD_Positor, 0) END,
            CASE
                WHEN i.CN_ADD_Contact_Address is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_ADD_Reliable) THEN
                    CASE i.CN_ADD_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_ADD_Reliability, 1)
            END
        FROM
            inserted i
    END
    IF(UPDATE(CN_PHN_ID))
        RAISERROR('The identity column CN_PHN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_PHN_CN_ID))
        RAISERROR('The foreign key column CN_PHN_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_PHN_Contact_Phone))
    BEGIN
        INSERT INTO [dbo].[CN_PHN_Contact_Phone] (
            CN_PHN_CN_ID,
            CN_PHN_Contact_Phone,
            CN_PHN_ChangedAt,
            CN_PHN_PositedAt,
            CN_PHN_Positor,
            CN_PHN_Reliability
        )
        SELECT
            ISNULL(i.CN_PHN_CN_ID, i.CN_ID),
            i.CN_PHN_Contact_Phone,
            cast(CASE
                WHEN i.CN_PHN_Contact_Phone is null THEN i.CN_PHN_ChangedAt
                WHEN UPDATE(CN_PHN_ChangedAt) THEN i.CN_PHN_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_PHN_PositedAt) THEN i.CN_PHN_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_PHN_Positor, 0) END,
            CASE
                WHEN i.CN_PHN_Contact_Phone is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_PHN_Reliable) THEN
                    CASE i.CN_PHN_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_PHN_Reliability, 1)
            END
        FROM
            inserted i
    END
    IF(UPDATE(CN_EML_ID))
        RAISERROR('The identity column CN_EML_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_EML_CN_ID))
        RAISERROR('The foreign key column CN_EML_CN_ID is not updatable.', 16, 1);
    IF(UPDATE(CN_EML_Contact_EMail))
    BEGIN
        INSERT INTO [dbo].[CN_EML_Contact_EMail] (
            CN_EML_CN_ID,
            CN_EML_Contact_EMail,
            CN_EML_ChangedAt,
            CN_EML_PositedAt,
            CN_EML_Positor,
            CN_EML_Reliability
        )
        SELECT
            ISNULL(i.CN_EML_CN_ID, i.CN_ID),
            i.CN_EML_Contact_EMail,
            cast(CASE
                WHEN i.CN_EML_Contact_EMail is null THEN i.CN_EML_ChangedAt
                WHEN UPDATE(CN_EML_ChangedAt) THEN i.CN_EML_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(CN_EML_PositedAt) THEN i.CN_EML_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.CN_EML_Positor, 0) END,
            CASE
                WHEN i.CN_EML_Contact_EMail is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(CN_EML_Reliable) THEN
                    CASE i.CN_EML_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.CN_EML_Reliability, 1)
            END
        FROM
            inserted i
    END
END
GO
-- DELETE trigger -----------------------------------------------------------------------------------------------------
-- dt_lCN_Contact instead of DELETE trigger on lCN_Contact
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[dt_lCN_Contact] ON [dbo].[lCN_Contact]
INSTEAD OF DELETE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    INSERT INTO [dbo].[CN_ADD_Contact_Address_Annex] (
        CN_ADD_ID,
        CN_ADD_Positor,
        CN_ADD_PositedAt,
        CN_ADD_Reliability
    )
    SELECT
        p.CN_ADD_ID,
        p.CN_ADD_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_ADD_Contact_Address_Annex] p
    ON
        p.CN_ADD_ID = d.CN_ADD_ID;
    INSERT INTO [dbo].[CN_PHN_Contact_Phone_Annex] (
        CN_PHN_ID,
        CN_PHN_Positor,
        CN_PHN_PositedAt,
        CN_PHN_Reliability
    )
    SELECT
        p.CN_PHN_ID,
        p.CN_PHN_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_PHN_Contact_Phone_Annex] p
    ON
        p.CN_PHN_ID = d.CN_PHN_ID;
    INSERT INTO [dbo].[CN_EML_Contact_EMail_Annex] (
        CN_EML_ID,
        CN_EML_Positor,
        CN_EML_PositedAt,
        CN_EML_Reliability
    )
    SELECT
        p.CN_EML_ID,
        p.CN_EML_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[CN_EML_Contact_EMail_Annex] p
    ON
        p.CN_EML_ID = d.CN_EML_ID;
END
GO
-- TIES ---------------------------------------------------------------------------------------------------------------
--
-- Ties are used to represent relationships between entities.
-- They come in four flavors: static, historized, knotted static, and knotted historized.
-- Ties have cardinality, constraining how members may participate in the relationship.
-- Every entity that is a member in a tie has a specified role in the relationship.
-- Ties must have at least two anchor roles and zero or more knot roles.
--
-- TIE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------------
--
-- The assembled view of a tie combines the posit and annex table of the tie.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- TIE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------------
--
-- These table valued functions rewind a tie posit table to the given
-- point in changing time, or a tie annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that 
-- their union corresponds to all rows in the posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- TIE TEMPORAL PERSPECTIVES ------------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each tie. There are four types of perspectives: latest,
-- point-in-time, difference, and now.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @reliable whether to show reliable (1) or unreliable (0) results
--
-- The latest perspective shows the latest available information for each tie.
-- The now perspective shows the information as it is right now.
-- The point-in-time perspective lets you travel through the information to the given timepoint.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
--
-- TIE TRIGGERS -------------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled and latest views make them behave like tables.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent ties, only changes that represent values different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Attribute sheet stack ----------------------------------------------------------------------------------------------
-- ssCN_ADD_Contact_Address procedure returning one temporal sheet per positor
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.ssCN_ADD_Contact_Address', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[ssCN_ADD_Contact_Address];
GO
CREATE PROCEDURE [dbo].[ssCN_ADD_Contact_Address] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_ADD_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_ADD_Contact_Address]
        WHERE
            CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_ADD_Positor
    FROM
        [dbo].[CN_ADD_Contact_Address]
    WHERE
        CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
    AND
        CN_ADD_Contact_Address is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_ADD_Positor,
                x.CN_ADD_CN_ID,
                x.CN_ADD_PositedAt,
                b.CN_ADD_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_ADD_CN_ID,
                    CN_ADD_PositedAt
                FROM
                    [dbo].[CN_ADD_Contact_Address]
                WHERE
                    CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_ADD_Positor,
                    CN_ADD_CN_ID,
                    CN_ADD_PositedAt,
                    CN_ADD_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_ADD_Contact_Address]
                PIVOT (
                    MIN(CN_ADD_Contact_Address) FOR CN_ADD_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_ADD_CN_ID = ISNULL(@CN_ID, CN_ADD_CN_ID)
                AND
                    CN_ADD_Positor = ' + @positor + '
            ) b
            ON
                b.CN_ADD_CN_ID = x.CN_ADD_CN_ID
            AND
                b.CN_ADD_PositedAt = x.CN_ADD_PositedAt
            ORDER BY
                CN_ADD_CN_ID,
                CN_ADD_PositedAt,
                CN_ADD_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END
GO
-- Attribute sheet stack ----------------------------------------------------------------------------------------------
-- ssCN_PHN_Contact_Phone procedure returning one temporal sheet per positor
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.ssCN_PHN_Contact_Phone', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[ssCN_PHN_Contact_Phone];
GO
CREATE PROCEDURE [dbo].[ssCN_PHN_Contact_Phone] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_PHN_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_PHN_Contact_Phone]
        WHERE
            CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_PHN_Positor
    FROM
        [dbo].[CN_PHN_Contact_Phone]
    WHERE
        CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
    AND
        CN_PHN_Contact_Phone is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_PHN_Positor,
                x.CN_PHN_CN_ID,
                x.CN_PHN_PositedAt,
                b.CN_PHN_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_PHN_CN_ID,
                    CN_PHN_PositedAt
                FROM
                    [dbo].[CN_PHN_Contact_Phone]
                WHERE
                    CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_PHN_Positor,
                    CN_PHN_CN_ID,
                    CN_PHN_PositedAt,
                    CN_PHN_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_PHN_Contact_Phone]
                PIVOT (
                    MIN(CN_PHN_Contact_Phone) FOR CN_PHN_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_PHN_CN_ID = ISNULL(@CN_ID, CN_PHN_CN_ID)
                AND
                    CN_PHN_Positor = ' + @positor + '
            ) b
            ON
                b.CN_PHN_CN_ID = x.CN_PHN_CN_ID
            AND
                b.CN_PHN_PositedAt = x.CN_PHN_PositedAt
            ORDER BY
                CN_PHN_CN_ID,
                CN_PHN_PositedAt,
                CN_PHN_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END
GO
-- Attribute sheet stack ----------------------------------------------------------------------------------------------
-- ssCN_EML_Contact_EMail procedure returning one temporal sheet per positor
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.ssCN_EML_Contact_EMail', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[ssCN_EML_Contact_EMail];
GO
CREATE PROCEDURE [dbo].[ssCN_EML_Contact_EMail] (
    @CN_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), CN_EML_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[CN_EML_Contact_EMail]
        WHERE
            CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        CN_EML_Positor
    FROM
        [dbo].[CN_EML_Contact_EMail]
    WHERE
        CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
    AND
        CN_EML_Contact_EMail is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.CN_EML_Positor,
                x.CN_EML_CN_ID,
                x.CN_EML_PositedAt,
                b.CN_EML_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    CN_EML_CN_ID,
                    CN_EML_PositedAt
                FROM
                    [dbo].[CN_EML_Contact_EMail]
                WHERE
                    CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
            ) x
            LEFT JOIN (
                SELECT
                    CN_EML_Positor,
                    CN_EML_CN_ID,
                    CN_EML_PositedAt,
                    CN_EML_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[CN_EML_Contact_EMail]
                PIVOT (
                    MIN(CN_EML_Contact_EMail) FOR CN_EML_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    CN_EML_CN_ID = ISNULL(@CN_ID, CN_EML_CN_ID)
                AND
                    CN_EML_Positor = ' + @positor + '
            ) b
            ON
                b.CN_EML_CN_ID = x.CN_EML_CN_ID
            AND
                b.CN_EML_PositedAt = x.CN_EML_PositedAt
            ORDER BY
                CN_EML_CN_ID,
                CN_EML_PositedAt,
                CN_EML_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END
GO
-- SCHEMA EVOLUTION ---------------------------------------------------------------------------------------------------
--
-- The following tables, views, and functions are used to track schema changes
-- over time, as well as providing every XML that has been 'executed' against
-- the database.
--
-- Schema table -------------------------------------------------------------------------------------------------------
-- The schema table holds every xml that has been executed against the database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema', 'U') IS NULL
   CREATE TABLE [dbo].[_Schema] (
      [version] int identity(1, 1) not null primary key,
      [activation] datetime2(7) not null,
      [schema] xml not null
   );
GO
-- Insert the XML schema (as of now)
INSERT INTO [dbo].[_Schema] (
   [activation],
   [schema]
)
SELECT
   current_timestamp,
   N'<schema format="0.98" date="2015-04-13" time="10:55:40"><metadata changingRange="datetime" encapsulation="dbo" identity="int" metadataPrefix="Metadata" metadataType="int" metadataUsage="false" changingSuffix="ChangedAt" identitySuffix="ID" positIdentity="int" positGenerator="true" positingRange="datetime" positingSuffix="PositedAt" positorRange="tinyint" positorSuffix="Positor" reliabilityRange="tinyint" reliabilitySuffix="Reliability" reliableCutoff="1" deleteReliability="0" reliableSuffix="Reliable" partitioning="false" entityIntegrity="true" restatability="true" idempotency="false" assertiveness="true" naming="improved" positSuffix="Posit" annexSuffix="Annex" chronon="datetime2(7)" now="sysdatetime()" dummySuffix="Dummy" versionSuffix="Version" statementTypeSuffix="StatementType" checksumSuffix="Checksum" businessViews="false" equivalence="false" equivalentSuffix="EQ" equivalentRange="tinyint" databaseTarget="SQLServer" temporalization="crt"/><anchor mnemonic="CN" descriptor="Contact" identity="int"><metadata capsule="dbo" generator="true"/><attribute mnemonic="ADD" descriptor="Address" identity="int" timeRange="datetime" dataRange="VARCHAR(128)"><metadata capsule="dbo" generator="false" assertive="true" restatable="false" idempotent="false"/><layout x="1022.76" y="460.20" fixed="true"/></attribute><attribute mnemonic="PHN" descriptor="Phone" identity="int" timeRange="datetime" dataRange="VARCHAR(20)"><metadata capsule="dbo" generator="false" assertive="true" restatable="false" idempotent="false"/><layout x="1012.23" y="605.15" fixed="true"/></attribute><attribute mnemonic="EML" descriptor="EMail" identity="int" timeRange="datetime" dataRange="VARCHAR(254)"><metadata capsule="dbo" generator="false" assertive="true" restatable="false" idempotent="false"/><layout x="879.49" y="638.44" fixed="true"/></attribute><layout x="960.00" y="549.50" fixed="false"/></anchor></schema>';
GO
-- Schema expanded view -----------------------------------------------------------------------------------------------
-- A view of the schema table that expands the XML attributes into columns
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema_Expanded', 'V') IS NOT NULL
DROP VIEW [dbo].[_Schema_Expanded]
GO
CREATE VIEW [dbo].[_Schema_Expanded]
AS
SELECT
	[version],
	[activation],
	[schema],
	[schema].value('schema[1]/@format', 'nvarchar(max)') as [format],
	[schema].value('schema[1]/@date', 'date') as [date],
	[schema].value('schema[1]/@time', 'time(0)') as [time],
	[schema].value('schema[1]/metadata[1]/@temporalization', 'nvarchar(max)') as [temporalization], 
	[schema].value('schema[1]/metadata[1]/@databaseTarget', 'nvarchar(max)') as [databaseTarget],
	[schema].value('schema[1]/metadata[1]/@changingRange', 'nvarchar(max)') as [changingRange],
	[schema].value('schema[1]/metadata[1]/@encapsulation', 'nvarchar(max)') as [encapsulation],
	[schema].value('schema[1]/metadata[1]/@identity', 'nvarchar(max)') as [identity],
	[schema].value('schema[1]/metadata[1]/@metadataPrefix', 'nvarchar(max)') as [metadataPrefix],
	[schema].value('schema[1]/metadata[1]/@metadataType', 'nvarchar(max)') as [metadataType],
	[schema].value('schema[1]/metadata[1]/@metadataUsage', 'nvarchar(max)') as [metadataUsage],
	[schema].value('schema[1]/metadata[1]/@changingSuffix', 'nvarchar(max)') as [changingSuffix],
	[schema].value('schema[1]/metadata[1]/@identitySuffix', 'nvarchar(max)') as [identitySuffix],
	[schema].value('schema[1]/metadata[1]/@positIdentity', 'nvarchar(max)') as [positIdentity],
	[schema].value('schema[1]/metadata[1]/@positGenerator', 'nvarchar(max)') as [positGenerator],
	[schema].value('schema[1]/metadata[1]/@positingRange', 'nvarchar(max)') as [positingRange],
	[schema].value('schema[1]/metadata[1]/@positingSuffix', 'nvarchar(max)') as [positingSuffix],
	[schema].value('schema[1]/metadata[1]/@positorRange', 'nvarchar(max)') as [positorRange],
	[schema].value('schema[1]/metadata[1]/@positorSuffix', 'nvarchar(max)') as [positorSuffix],
	[schema].value('schema[1]/metadata[1]/@reliabilityRange', 'nvarchar(max)') as [reliabilityRange],
	[schema].value('schema[1]/metadata[1]/@reliabilitySuffix', 'nvarchar(max)') as [reliabilitySuffix],
	[schema].value('schema[1]/metadata[1]/@reliableCutoff', 'nvarchar(max)') as [reliableCutoff],
	[schema].value('schema[1]/metadata[1]/@deleteReliability', 'nvarchar(max)') as [deleteReliability],
	[schema].value('schema[1]/metadata[1]/@reliableSuffix', 'nvarchar(max)') as [reliableSuffix],
	[schema].value('schema[1]/metadata[1]/@partitioning', 'nvarchar(max)') as [partitioning],
	[schema].value('schema[1]/metadata[1]/@entityIntegrity', 'nvarchar(max)') as [entityIntegrity],
	[schema].value('schema[1]/metadata[1]/@restatability', 'nvarchar(max)') as [restatability],
	[schema].value('schema[1]/metadata[1]/@idempotency', 'nvarchar(max)') as [idempotency],
	[schema].value('schema[1]/metadata[1]/@assertiveness', 'nvarchar(max)') as [assertiveness],
	[schema].value('schema[1]/metadata[1]/@naming', 'nvarchar(max)') as [naming],
	[schema].value('schema[1]/metadata[1]/@positSuffix', 'nvarchar(max)') as [positSuffix],
	[schema].value('schema[1]/metadata[1]/@annexSuffix', 'nvarchar(max)') as [annexSuffix],
	[schema].value('schema[1]/metadata[1]/@chronon', 'nvarchar(max)') as [chronon],
	[schema].value('schema[1]/metadata[1]/@now', 'nvarchar(max)') as [now],
	[schema].value('schema[1]/metadata[1]/@dummySuffix', 'nvarchar(max)') as [dummySuffix],
	[schema].value('schema[1]/metadata[1]/@statementTypeSuffix', 'nvarchar(max)') as [statementTypeSuffix],
	[schema].value('schema[1]/metadata[1]/@checksumSuffix', 'nvarchar(max)') as [checksumSuffix],
	[schema].value('schema[1]/metadata[1]/@businessViews', 'nvarchar(max)') as [businessViews],
	[schema].value('schema[1]/metadata[1]/@equivalence', 'nvarchar(max)') as [equivalence],
	[schema].value('schema[1]/metadata[1]/@equivalentSuffix', 'nvarchar(max)') as [equivalentSuffix],
	[schema].value('schema[1]/metadata[1]/@equivalentRange', 'nvarchar(max)') as [equivalentRange]
FROM 
	_Schema;
GO
-- Anchor view --------------------------------------------------------------------------------------------------------
-- The anchor view shows information about all the anchors in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Anchor', 'V') IS NOT NULL
DROP VIEW [dbo].[_Anchor]
GO
CREATE VIEW [dbo].[_Anchor]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.anchor.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.anchor.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.anchor.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.anchor.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.anchor.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.anchor.value('count(attribute)', 'int') as [numberOfAttributes]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as Nodeset(anchor);
GO
-- Knot view ----------------------------------------------------------------------------------------------------------
-- The knot view shows information about all the knots in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Knot', 'V') IS NOT NULL
DROP VIEW [dbo].[_Knot]
GO
CREATE VIEW [dbo].[_Knot]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.knot.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.knot.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.knot.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.knot.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.knot.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.knot.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.knot.value('@dataRange', 'nvarchar(max)') as [dataRange],
   isnull(Nodeset.knot.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   isnull(Nodeset.knot.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/knot') as Nodeset(knot);
GO
-- Attribute view -----------------------------------------------------------------------------------------------------
-- The attribute view shows information about all the attributes in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Attribute', 'V') IS NOT NULL
DROP VIEW [dbo].[_Attribute]
GO
CREATE VIEW [dbo].[_Attribute]
AS
SELECT
   S.version,
   S.activation,
   ParentNodeset.anchor.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   ParentNodeset.anchor.value('concat(@descriptor, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [name],
   Nodeset.attribute.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.attribute.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.attribute.value('@identity', 'nvarchar(max)') as [identity],
   isnull(Nodeset.attribute.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent],
   Nodeset.attribute.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.attribute.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   isnull(Nodeset.attribute.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   Nodeset.attribute.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.attribute.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent],
   ParentNodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [anchorMnemonic],
   ParentNodeset.anchor.value('@descriptor', 'nvarchar(max)') as [anchorDescriptor],
   ParentNodeset.anchor.value('@identity', 'nvarchar(max)') as [anchorIdentity],
   Nodeset.attribute.value('@dataRange', 'nvarchar(max)') as [dataRange],
   Nodeset.attribute.value('@knotRange', 'nvarchar(max)') as [knotRange],
   Nodeset.attribute.value('@timeRange', 'nvarchar(max)') as [timeRange]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as ParentNodeset(anchor)
OUTER APPLY
   ParentNodeset.anchor.nodes('attribute') as Nodeset(attribute);
GO
-- Tie view -----------------------------------------------------------------------------------------------------------
-- The tie view shows information about all the ties in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Tie', 'V') IS NOT NULL
DROP VIEW [dbo].[_Tie]
GO
CREATE VIEW [dbo].[_Tie]
AS
SELECT
   S.version,
   S.activation,
   REPLACE(Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return concat($role/@type, "_", $role/@role)
   ').value('.', 'nvarchar(max)'), ' ', '_') as [name],
   Nodeset.tie.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.tie.value('count(anchorRole) + count(knotRole)', 'int') as [numberOfRoles],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return string($role/@role)
   ').value('.', 'nvarchar(max)') as [roles],
   Nodeset.tie.value('count(anchorRole)', 'int') as [numberOfAnchors],
   Nodeset.tie.query('
      for $role in anchorRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [anchors],
   Nodeset.tie.value('count(knotRole)', 'int') as [numberOfKnots],
   Nodeset.tie.query('
      for $role in knotRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [knots],
   Nodeset.tie.value('count(*[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"])', 'int') as [numberOfIdentifiers],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"]
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [identifiers],
   Nodeset.tie.value('@timeRange', 'nvarchar(max)') as [timeRange],
   Nodeset.tie.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.tie.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   Nodeset.tie.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.tie.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/tie') as Nodeset(tie);
GO
-- Evolution function -------------------------------------------------------------------------------------------------
-- The evolution function shows what the schema looked like at the given
-- point in time with additional information about missing or added
-- modeling components since that time.
--
-- @timepoint The point in time to which you would like to travel.
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Evolution', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[_Evolution];
GO
CREATE FUNCTION [dbo].[_Evolution] (
    @timepoint AS DATETIME2(7)
)
RETURNS TABLE
RETURN
SELECT
   V.[version],
   ISNULL(S.[name], T.[name]) AS [name],
   ISNULL(V.[activation], T.[create_date]) AS [activation],
   CASE
      WHEN S.[name] is null THEN
         CASE
            WHEN T.[create_date] > (
               SELECT
                  ISNULL(MAX([activation]), @timepoint)
               FROM
                  [dbo].[_Schema]
               WHERE
                  [activation] <= @timepoint
            ) THEN 'Future'
            ELSE 'Past'
         END
      WHEN T.[name] is null THEN 'Missing'
      ELSE 'Present'
   END AS Existence
FROM (
   SELECT
      MAX([version]) as [version],
      MAX([activation]) as [activation]
   FROM
      [dbo].[_Schema]
   WHERE
      [activation] <= @timepoint
) V
JOIN (
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Anchor] a
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Knot] k
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Attribute] b
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Tie] t
) S
ON
   S.[version] = V.[version]
FULL OUTER JOIN (
   SELECT
      [name],
      [create_date]
   FROM
      sys.tables
   WHERE
      [type] like '%U%'
   AND
      LEFT([name], 1) <> '_'
) T
ON
   S.[name] = T.[name];
GO
-- Drop Script Generator ----------------------------------------------------------------------------------------------
-- generates a drop script, that must be run separately, dropping everything in an Anchor Modeled database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateDropScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateDropScript];
GO
CREATE PROCEDURE [dbo]._GenerateDropScript (
   @exclusionPattern varchar(42) = '[_]%', -- exclude Metadata by default
   @inclusionPattern varchar(42) = '%' -- include everything by default
)
AS
BEGIN
   DECLARE @xml XML;
   WITH objects AS (
      SELECT
         'DROP ' + ft.[type] + ' ' + fn.[name] + '; -- ' + fn.[description] as [statement],
         row_number() OVER (
            ORDER BY
               -- restatement finders last
               CASE dc.[description]
                  WHEN 'restatement finder' THEN 1
                  ELSE 0
               END ASC,
               -- order based on type
               CASE ft.[type]
                  WHEN 'PROCEDURE' THEN 1
                  WHEN 'FUNCTION' THEN 2
                  WHEN 'VIEW' THEN 3
                  WHEN 'TABLE' THEN 4
                  ELSE 5
               END ASC,
               -- order within type
               CASE dc.[description]
                  WHEN 'key generator' THEN 1
                  WHEN 'latest perspective' THEN 2
                  WHEN 'current perspective' THEN 3
                  WHEN 'difference perspective' THEN 4
                  WHEN 'point-in-time perspective' THEN 5
                  WHEN 'time traveler' THEN 6
                  WHEN 'rewinder' THEN 7
                  WHEN 'assembled view' THEN 8
                  WHEN 'annex table' THEN 9
                  WHEN 'posit table' THEN 10
                  WHEN 'table' THEN 11
                  WHEN 'restatement finder' THEN 12
                  ELSE 13
               END,
               -- order within description
               CASE ft.[type]
                  WHEN 'TABLE' THEN
                     CASE cl.[class]
                        WHEN 'Attribute' THEN 1
                        WHEN 'Attribute Annex' THEN 2
                        WHEN 'Attribute Posit' THEN 3
                        WHEN 'Tie' THEN 4
                        WHEN 'Anchor' THEN 5
                        WHEN 'Knot' THEN 6
                        ELSE 7
                     END
                  ELSE
                     CASE cl.[class]
                        WHEN 'Anchor' THEN 1
                        WHEN 'Attribute' THEN 2
                        WHEN 'Attribute Annex' THEN 3
                        WHEN 'Attribute Posit' THEN 4
                        WHEN 'Tie' THEN 5
                        WHEN 'Knot' THEN 6
                        ELSE 7
                     END
               END,
               -- finally alphabetically
               o.[name] ASC
         ) AS [ordinal]
      FROM
         sys.objects o
      JOIN
         sys.schemas s
      ON
         s.[schema_id] = o.[schema_id]
      AND
         s.[name] = 'dbo'
      CROSS APPLY (
         SELECT
            CASE
               WHEN o.[name] LIKE '[_]%'
               COLLATE Latin1_General_BIN THEN 'Metadata'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][a-z]%[A-Z][A-Z][_][a-z]%'
               COLLATE Latin1_General_BIN THEN 'Tie'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][A-Z][A-Z][A-Z][_][A-Z]%[_]%'
               COLLATE Latin1_General_BIN THEN 'Attribute'
               WHEN o.[name] LIKE '%[A-Z][A-Z][A-Z][_][A-Z]%'
               COLLATE Latin1_General_BIN THEN 'Knot'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][A-Z]%'
               COLLATE Latin1_General_BIN THEN 'Anchor'
               ELSE 'Other'
            END
      ) cl ([class])
      CROSS APPLY (
         SELECT
            CASE o.[type]
               WHEN 'P' THEN 'PROCEDURE'
               WHEN 'IF' THEN 'FUNCTION'
               WHEN 'FN' THEN 'FUNCTION'
               WHEN 'V' THEN 'VIEW'
               WHEN 'U' THEN 'TABLE'
            END
      ) ft ([type])
      CROSS APPLY (
         SELECT
            CASE
               WHEN ft.[type] = 'PROCEDURE' AND cl.[class] = 'Anchor' AND o.[name] LIKE 'k%'
               COLLATE Latin1_General_BIN THEN 'key generator'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 't%'
               COLLATE Latin1_General_BIN THEN 'time traveler'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'rf%'
               COLLATE Latin1_General_BIN THEN 'restatement finder'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'r%'
               COLLATE Latin1_General_BIN THEN 'rewinder'
               WHEN ft.[type] = 'VIEW' AND o.[name] LIKE 'l%'
               COLLATE Latin1_General_BIN THEN 'latest perspective'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'p%'
               COLLATE Latin1_General_BIN THEN 'point-in-time perspective'
               WHEN ft.[type] = 'VIEW' AND o.[name] LIKE 'n%'
               COLLATE Latin1_General_BIN THEN 'current perspective'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'd%'
               COLLATE Latin1_General_BIN THEN 'difference perspective'
               WHEN ft.[type] = 'VIEW' AND cl.[class] = 'Attribute'
               COLLATE Latin1_General_BIN THEN 'assembled view'
               WHEN ft.[type] = 'TABLE' AND o.[name] LIKE '%Annex'
               COLLATE Latin1_General_BIN THEN 'annex table'
               WHEN ft.[type] = 'TABLE' AND o.[name] LIKE '%Posit'
               COLLATE Latin1_General_BIN THEN 'posit table'
               WHEN ft.[type] = 'TABLE'
               COLLATE Latin1_General_BIN THEN 'table'
               ELSE 'other'
            END
      ) dc ([description])
      CROSS APPLY (
         SELECT
            s.[name] + '.' + o.[name],
            cl.[class] + ' ' + dc.[description]
      ) fn ([name], [description])
      WHERE
         o.[type] IN ('P', 'IF', 'FN', 'V', 'U')
      AND
         o.[name] NOT LIKE ISNULL(@exclusionPattern, '')
      AND
         o.[name] LIKE ISNULL(@inclusionPattern, '%')
   )
   SELECT @xml = (
       SELECT
          [statement] + CHAR(13) as [text()]
       FROM
          objects
       ORDER BY
          [ordinal]
       FOR XML PATH('')
   );
   SELECT @xml;
END
GO
-- Database Copy Script Generator -------------------------------------------------------------------------------------
-- generates a copy script, that must be run separately, copying all data between two identically modeled databases
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateCopyScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateCopyScript];
GO
CREATE PROCEDURE [dbo]._GenerateCopyScript (
	@source varchar(123),
	@target varchar(123)
)
as 
begin
	declare @R char(1) = CHAR(13);
	-- stores the built SQL code
	declare @sql varchar(max) = 'USE ' + @target + ';' + @R;
	declare @xml xml;
	-- find which version of the schema that is in effect
	declare @version int;
	select 
		@version = max([version]) 
	from
		_Schema;
	-- declare and set other variables we need
	declare @equivalentSuffix varchar(42);
	declare @identitySuffix varchar(42);
	declare @annexSuffix varchar(42);
	declare @positSuffix varchar(42);
	declare @temporalization varchar(42);
	select
		@equivalentSuffix = equivalentSuffix,
		@identitySuffix = identitySuffix,
		@annexSuffix = annexSuffix,
		@positSuffix = positSuffix,
		@temporalization = temporalization
	from
		_Schema_Expanded 
	where
		[version] = @version;
	-- build non-equivalent knot copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R 
			end
		from 
			_Knot x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'false'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build equivalent knot copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @identitySuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @identitySuffix + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' OFF;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + ';' + @R
		from 
			_Knot x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'true'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build anchor copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R 
			end
		from 
			_Anchor x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build attribute copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select 
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from 
				_Attribute x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select 
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from 
				_Attribute x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build tie copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select 
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from 
				_Tie x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select 
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from 
				_Tie x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	select @sql for xml path('');
end
-- DESCRIPTIONS -------------------------------------------------------------------------------------------------------

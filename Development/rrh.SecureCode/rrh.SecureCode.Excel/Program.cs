﻿using System;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;

namespace rrh.SecureCode
{
	class Program
	{
		static void Main()
		{
			Excel.Application xla = new Excel.Application();

			string Path = Directory.GetCurrentDirectory() + "\\OSIM2015 Template 20150504.1.xlsm";

			Excel.Workbook wkb = xla.Workbooks.Open(Path);

			long num = LoopNumeric(wkb);
			long obj = LoopObject(wkb);

			double inc = ((double)num - (double)obj) / (double)num * 100.0;

			wkb.Close(false);
			wkb = null;

			xla.Quit();
			xla = null;

			Console.WriteLine();
			Console.WriteLine("Numeric duration (Ticks): {0}", num);
			Console.WriteLine("Object duration  (Ticks): {0}", obj);
			Console.WriteLine();
			Console.WriteLine("Object takes {0} % of numeric duration", Math.Round(inc, 2));

			Console.WriteLine();
			Console.WriteLine("Press Any Key to Continue.");
			Console.ReadKey();
		}

		private static long LoopNumeric(Excel.Workbook wkb)
		{
			DateTime beg = DateTime.Now;
			
			for (int i = 1; i <= wkb.Sheets.Count; i++)
			{
				Console.WriteLine(wkb.Sheets[i].Name);
			}

			DateTime end = DateTime.Now;

			return (end.Ticks - beg.Ticks);
		}

		private static long LoopObject(Excel.Workbook wkb)
		{
			DateTime beg = DateTime.Now;

			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				Console.WriteLine(wks.Name);
			}

			DateTime end = DateTime.Now;

			return(end.Ticks - beg.Ticks);
		}
	}
}

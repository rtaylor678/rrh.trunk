﻿using System;
using System.IO;
using System.Security;
using System.Security.Permissions;

namespace rrh.SecureCode.Library
{
	public static class FileAccessSecurity
	{
		public static string FileContentsRisky(string path)
		{
			try
			{
				return File.ReadAllText(path);
			}
			catch
			{
				return ("**** Could not read file: " + path);
			}
		}

		[FileIOPermission(SecurityAction.Assert, AllLocalFiles = FileIOPermissionAccess.Read)]
		public static string FileContentsLocalFiles(string path)
		{
			return FileContentsRisky(path);
		}

		[FileIOPermission(SecurityAction.Assert, Read = @"C:\CPA_20130129")]
		public static string FileContentsFolder(string path)
		{
			return FileContentsRisky(path);
		}

		[FileIOPermission(SecurityAction.Assert, Read = @"C:\TestFile.Local.txt")]
		public static string FileContentsSpecific(string path)
		{
			return FileContentsRisky(path);
		}
	}
}
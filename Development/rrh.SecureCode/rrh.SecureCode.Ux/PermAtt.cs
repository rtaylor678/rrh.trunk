﻿using System;
using System.IO;
using System.Security;
using System.Security.Permissions;

using rrh.SecureCode.Library;

namespace rrh.SecureCode.Ux
{
	[FileIOPermission(SecurityAction.PermitOnly, AllFiles = FileIOPermissionAccess.NoAccess)]
	class PermAtt
	{
		static void Main()
		{
			FileAccessTests();

			Console.WriteLine("Press Any Key to Continue.");
			Console.ReadKey();
		}

		private static void FileAccessTests()
		{
			string path = @"C:\TestFile.Local.txt";

			Console.WriteLine("Local Risky:      " + FileAccessSecurity.FileContentsRisky(path));
			Console.WriteLine();

			Console.WriteLine("Local Secure:     " + FileAccessSecurity.FileContentsLocalFiles(path));
			Console.WriteLine();
			
			Console.WriteLine("Local Folder:     " + FileAccessSecurity.FileContentsFolder(path));
			Console.WriteLine();
			
			Console.WriteLine("Local Specific:   " + FileAccessSecurity.FileContentsSpecific(path));
			Console.WriteLine();

			path = @"R:\TestFile.Network.txt";

			Console.WriteLine("Network Risky:    " + FileAccessSecurity.FileContentsRisky(path));
			Console.WriteLine();

			Console.WriteLine("Network Secure:   " + FileAccessSecurity.FileContentsLocalFiles(path));
			Console.WriteLine();

			Console.WriteLine("Network Folder:   " + FileAccessSecurity.FileContentsFolder(path));
			Console.WriteLine();

			Console.WriteLine("Network Specific: " + FileAccessSecurity.FileContentsSpecific(path));
			Console.WriteLine();
		}
	}
}
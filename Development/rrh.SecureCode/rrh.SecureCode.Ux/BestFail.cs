﻿using System;
using System.IO;

namespace rrh.SecureCode.Ux
{
	class BestFail
	{
		static void Main()
		{
			string path = @"C:\TestFile.Local.txt";

			if (IsClient())
			{
				Console.WriteLine("Client:   " + FileReader(path));
				Console.WriteLine();
			}
			else
			{
				Console.WriteLine("Solomon:  " + FileReader(path));
				Console.WriteLine();
			}

			Console.WriteLine("Press Any Key to Continue.");
			Console.ReadKey();
		}

		private static Boolean IsClient()
		{
			bool b;

			b = (Environment.MachineName.Substring(1, 5) != "THINK");
			b = (DateTime.Today.DayOfWeek == DayOfWeek.Sunday);

			return (b);
		}

		public static string FileReader(string path)
		{
			try
			{
				return File.ReadAllText(path);
			}
			catch
			{
				return ("**** Could not read file: " + path);
			}
		}
	}
}
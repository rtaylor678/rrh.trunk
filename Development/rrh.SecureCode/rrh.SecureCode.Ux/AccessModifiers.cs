﻿using System;
using System.IO;

namespace rrh.SecureCode.Ux
{
	class AccessModifiers
	{
		static void Main()
		{
			SolomonFileReader SolomonFR = new SolomonFileReader();
			ClientFileReader ClientFR = new ClientFileReader();
			
			string path = @"C:\TestFile.Local.txt";

			if (!IsClient())
			{
				Console.WriteLine("Solomon Public:    " + SolomonFR.SolomonRdrPublic(path));
				Console.WriteLine("Solomon Internal:  " + SolomonFR.SolomonRdrInternal(path));
				//Console.WriteLine("Solomon Protected: " + SolomonFR.ContentsProtected(path));
				//Console.WriteLine("Solomon Private:   " + SolomonFR.ContentsPrivate(path));
				Console.WriteLine();

				Console.WriteLine("Client  Public:    " + ClientFR.ClientRdrPublic(path));
				Console.WriteLine("Client  Internal:  " + ClientFR.ClientRdrInternal(path));
				//Console.WriteLine("Client Protected:  " + ClientFR.ClientRdrProtected(path));
				//Console.WriteLine("Client Private:    " + ClientFR.ClientRdrPrivate(path));
				Console.WriteLine();
			}
			else
			{
				Console.WriteLine("Client Computer Do Not Read Files!!");
				Console.WriteLine();
			}

			Console.WriteLine("Press Any Key to Continue.");
			Console.ReadKey();
		}

		[Obsolete("Please use 'Solomon'.", false)]
		private static Boolean IsClient()
		{
			return (Environment.MachineName.Substring(0, 5) != "THINK");
		}

		private static Boolean IsSolomon()
		{
			return (Environment.MachineName.Substring(0, 5) == "THINK");
		}

		private class ClientFileReader : SolomonFileReader
		{
			public string ClientRdrPublic(string path)
			{
				return SolomonRdrPublic(path);
			}

			internal string ClientRdrInternal(string path)
			{
				return SolomonRdrInternal(path);
			}

			protected string ClientRdrProtected(string path)
			{
				return SolomonRdrProtected(path);
			}

			//private string ClientRdrPrivate(string path)
			//{
			//	return SolomonRdrPrivate(path);
			//}
		}

		private class SolomonFileReader
		{
			public string SolomonRdrPublic(string path)
			{
				return SolomonRdrPrivate(path);
			}

			internal string SolomonRdrInternal(string path)
			{
				return SolomonRdrPrivate(path);
			}

			protected string SolomonRdrProtected(string path)
			{
				return SolomonRdrPrivate(path);
			}

			private string SolomonRdrPrivate(string path)
			{
				try
				{
					return File.ReadAllText(path);
				}
				catch
				{
					return ("**** Could not read file: " + path);
				}
			}
		}
	}
}